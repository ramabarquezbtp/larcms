<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('help/{file}', 'admin\HelpController@index');

Route::resource('admin/home', 'admin\HomeController');
Route::resource('admin/', 'admin\HomeController');

Route::resource('admin/pages', 'admin\PageController');
Route::post('admin/pages/save-changes/', 'admin\PageController@save_changes');
Route::post('admin/pages/delete/', 'admin\PageController@remove');

Route::resource('admin/forms', 'admin\FormController');
Route::post('admin/forms/save-changes/', 'admin\FormController@save_changes');
Route::post('admin/forms/delete/', 'admin\FormController@remove');
Route::post('admin/forms/temp-store', 'admin\FormController@storeSession');
Route::get('admin/forms/create/{form_page_index?}', 'admin\FormController@create');
Route::get('admin/forms/{id}/edit/{page?}', 'admin\FormController@edit');
Route::get('admin/forms/remove-form/{page?}/{id?}', 'admin\FormController@removeForm');
Route::get('admin/forms/test/test', 'admin\FormController@testMode');
Route::get('admin/forms/add-page/{index}/{id?}', 'admin\FormController@addPage');
Route::get('admin/forms/submissions/{form_id}', 'admin\FormController@formSubmission');
Route::get('admin/forms/submissions/view/{id}', 'admin\FormController@submissionView');

Route::resource('admin/contents', 'admin\ContentController');
Route::post('admin/contents/save-changes/', 'admin\ContentController@save_changes');
Route::post('admin/contents/delete/', 'admin\ContentController@remove');
Route::post('admin/contents/find-n-replace', 'admin\ContentController@find_and_replace');
Route::post('admin/contents/sort/{field?}/{order?}', 'admin\ContentController@sort');

Route::resource('admin/files', 'admin\FileController');
Route::post('admin/files/save-changes/', 'admin\FileController@save_changes');
Route::post('admin/files/delete/', 'admin\FileController@remove');

Route::resource('admin/users', 'admin\UserController');
Route::post('admin/users/save-changes/', 'admin\UserController@save_changes');
Route::post('admin/users/delete/', 'admin\UserController@remove');

Route::resource('admin/events', 'admin\EventController');
Route::post('admin/events/save-changes/', 'admin\EventController@save_changes');
Route::post('admin/events/delete/', 'admin\EventController@remove');


Route::resource('admin/galleries', 'admin\GalleryController');
Route::post('admin/galleries/save-changes/', 'admin\GalleryController@save_changes');
Route::post('admin/galleries/delete/', 'admin\GalleryController@remove');
Route::get('admin/galleries/view/gallery/{id}/{i}', 'admin\GalleryController@view_gallery');

Route::get('files/image/{file_id}', 'PublicController@image');
Route::get('files/thumbnail/{file_id}', 'PublicController@thumbnail');
Route::get('files/doc/{file_id}/{download?}', 'PublicController@doc');
Route::get('page/{page_id?}', 'PublicController@page');
Route::get('public/testing', 'PublicController@testing');
Route::post('form/submit', 'PublicController@save_form');
Route::post('download_file', 'PublicController@download_file');
Route::get('search', 'PublicController@find_keyword');
Route::get('news-events', 'PublicController@news_events');
Route::get('news-events/{uri}', 'PublicController@news_events_view');

Route::get('/', 'PublicController@page');

Route::get('portal/{page_id?}', 'admin\PortalController@page');

/*Route::get('/', function () {
    return view('auth.login');
});*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('test-this',function(){
    //return \App\lib\MyMail::send('ram.abarquez.nettrac@gmail.com','Ramonito Aba', 'Calculus','body and soul');
});