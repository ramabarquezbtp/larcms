<script>
    // when form is submit

    var base64Global = '';

    $('#form-wizard').submit(function() {

        event.preventDefault();

        var index_name = '';

        var has_file = false;


        $("#submit-form").removeClass("btn-primary").addClass("btn-success").attr('disabled','disabled').val('Sending Form...').css('font-style','italic');

        //return false;

        if($('input[type="file"]').val() && $('input[type="file"]').attr('name')){
            var file = document.querySelector('input[type="file"]').files[0];
            getBase64(file);
            index_name = $('input[type="file"]').attr('name');
            has_file = true;
        }

        var action = $(this).attr('action');


        // needs for recaptacha ready
        grecaptcha.ready(function() {
            // do request for recaptcha token
            // response is promise with passed token
            grecaptcha.execute('{{env('SITE_KEY')}}', {action: 'create_comment'}).then(function(token) {
                // add token to form
                $('#form-wizard').prepend('<input type="hidden" name="g-recaptcha-response" value="' + token + '">');

                if(has_file){

                    while (base64Global == ''){

                    };

                    $('#form-wizard').prepend('<textarea type="hidden" name="'+index_name+'" style="display: none">'+base64Global+'</textarea>');
                }



                $('#form-wizard').prepend('<input type="hidden" name="token" value="' + token + '">');



                var $form = $("#form-wizard");
                var data = getFormData($form);

                //data['base64'] = base64;

                //alert(base64Global);

                //alert(JSON.stringify(data));
                //return false;


                //$("#form-wizard").submit();

                $.post(action, data, function(result) {

                    console.log(result);

                    if(result.success) {
                        window.location = "{{url('page/thank-you')}}";
                    } else {
                        alert('You are spammer ! Get the @$%K out.');
                        $("#submit-form").removeClass("btn-success").addClass('btn-danger').val('Error Sending...')
                    }
                },'json');
            });;
        });
    });


    function getBase64(file) {
        var reader = new FileReader();
        reader.readAsDataURL(file);



        reader.onload = function () {
            base64Global = reader.result;
            console.log(reader.result);
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
            return false;
        };


    }




    function getFormData($form){
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};

        $.map(unindexed_array, function(n, i){
            indexed_array[n['name']] = n['value'];
        });

        return indexed_array;
    }

</script>