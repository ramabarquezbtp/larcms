<?php
$images_list = [];

if(\App\Model\File::count()){
    foreach (\App\Model\File::whereIn('mime_type',config('app.pic_types'))->get() as $row){
        $images_list[] = "'/files/image/$row->id'";
    }
}

?>

<script type="text/javascript" src="/assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

    var cssA = [
        '/assets/theme1/wp-content/themes/wddswp/css/bootstrap.min.css',
        '/assets/theme1/wp-content/themes/wddswp/css/jquery-ui.min.css',
        'https://use.fontawesome.com/releases/v5.1.0/css/all.css',
        'https://fonts.googleapis.com/css?family=Raleway:400,600',
        'https://fonts.googleapis.com/css?family=Adamina',
        'https://fonts.googleapis.com/css?family=Muli:400,600,700,900',
        'https://f1-na.readspeaker.com/script/7304/ReadSpeaker.Styles.css?v=2.5.13.5528',
        '/assets/theme1/wp-content/plugins/slick-sitemap/slickmap.css%3Fver=4.9.8.css',
        '/assets/theme1/wp-content/plugins/all-in-one-event-calendar/public/themes-ai1ec/vortex/css/ai1ec_parsed_css.css%3Fver=2.5.36.css',
        '/assets/theme1/wp-content/plugins/photo-gallery/css/sumoselect.min.css%3Fver=3.0.3.css',
        '/assets/theme1/wp-content/plugins/photo-gallery/css/font-awesome/font-awesome.min.css%3Fver=4.6.3.css',
        '/assets/theme1/wp-content/plugins/photo-gallery/css/jquery.mCustomScrollbar.min.css%3Fver=1.5.13.css',
        '/assets/theme1/wp-includes/css/dashicons.min.css%3Fver=4.9.8.css',
        'https://fonts.googleapis.com/css?family=Ubuntu&subset=greek,latin,greek-ext,vietnamese,cyrillic-ext,latin-ext,cyrillic',
        '/assets/theme1/wp-content/plugins/photo-gallery/css/bwg_frontend.css%3Fver=1.5.13.css',
        '/assets/theme1/wp-content/plugins/rating-widget/resources/css/site-rating.css%3Fver=3.0.3.css',
        '/assets/theme1/wp-content/plugins/user-access-manager/assets/css/uamLoginForm.css%3Fver=2.1.11.css',
        '/assets/theme1/wp-content/plugins/wp-accessibility/css/wpa-style.css%3Fver=4.9.8.css',
        '/assets/theme1/wp-content/plugins/wp-accessibility/toolbar/fonts/css/a11y-toolbar.css%3Fver=4.9.8.css',
        '/assets/theme1/wp-content/plugins/wp-accessibility/toolbar/css/a11y.css%3Fver=4.9.8.css',
        '/assets/theme1/wp-content/plugins/wp-accessibility/toolbar/css/a11y-fontsize.css%3Fver=4.9.8.css',
        '/assets/theme1/wp-content/plugins/wp-blog-and-widgets/css/styleblog.css%3Fver=1.6.css',
        'https://fonts.googleapis.com/css?family=Noto+Sans%3A400italic%2C700italic%2C400%2C700%7CNoto+Serif%3A400italic%2C700italic%2C400%2C700%7CInconsolata%3A400%2C700&subset=latin%2Clatin-ext',
        '/assets/theme1/wp-content/themes/wddswp/genericons/genericons.css%3Fver=3.2.css',
        '/assets/theme1/wp-content/themes/wddswp/style.css%3Fver=4.9.8.css',
        'https://secure.rating-widget.com/css/wordpress/toprated.css?ck=Y2019M04D29&ver=3.0.3',
        'https://secure.rating-widget.com/css/widget/recommendations.css?ck=Y2019M04D29&ver=3.0.3',
        '/assets/theme1/wp-content/plugins/sassy-social-share/public/css/sassy-social-share-public.css%3Fver=3.2.10.css',
        '/assets/theme1/wp-content/plugins/sassy-social-share/admin/css/sassy-social-share-svg.css%3Fver=3.2.10.css',
        '/assets/theme1/wp-content/plugins/google-calendar-events/assets/css/vendor/jquery.qtip.min.css%3Fver=3.1.20.css',
        '/assets/theme1/wp-content/plugins/google-calendar-events/assets/css/default-calendar-grid.min.css%3Fver=3.1.20.css',
        '/assets/theme1/wp-content/plugins/google-calendar-events/assets/css/default-calendar-list.min.css%3Fver=3.1.20.css',
        '/assets/theme1/wp-content/plugins/js_composer/assets/css/js_composer.min.css%3Fver=5.4.7.css',
        '/assets/theme1/wp-content/uploads/js_composer/custom.css%3Fver=5.4.7.css',
        '/assets/theme1/custom.css',
        '//css.rating-widget.com/widget/style.min.css?v=2.1.7'
    ]


    CKEDITOR.editorConfig = function( config )
    {
        config.enterMode = CKEDITOR.ENTER_BR;
        config.shiftEnterMode = CKEDITOR.ENTER_BR;
    };



    CKEDITOR.on( 'instanceCreated', function( e ){

        $.each(cssA,function(i,v){
            e.editor.addContentsCss(v);
        });

    });


    CKEDITOR.replace('content');

    var text = 'the price is good';

    var keyword = 'pricetag';

    var thisRegex = new RegExp(keyword);

    var ret = false;

    if(thisRegex.test(text)){
        //alert('alert');

    }



    var start_index_sd = -1;

    var list_dp = [{!! html_entity_decode(implode(",",$images_list)) !!}];

    var list_size = list_dp.length;


    function matchPhrase(keyword, text) {

        var thisRegex = new RegExp(keyword);

        var ret = false;

        if(thisRegex.test(text)){
            ret = true;
        }

        return ret;
    }


    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    var global_ctr = 0;
    var isset_sdp = 0;

    setInterval(function(){
        global_ctr++;

        $('label:contains("URL")').siblings('div').children('div').children('input').each(function(){

            var id_input = $(this).attr('id');

            if(typeof id_input === 'string' && !$("#"+id_input).attr('sdp')){
                isset_sdp = 1;
                init_search_dp("#"+id_input);
                $("#"+id_input).attr('sdp','1')
                console.log(id_input);
            }
        });

        var row_li = $("ul.drop-search li").attr('class');

        $('li[class^="list-"]').each(function(){

            if(!$(this).attr('hasOnClick')){
                $(this).attr('hasOnclick','1');

                $(this).hover(function(){
                    $('li[class^="list-"]').removeClass('selected');
                    $(this).addClass('selected');
                    $(this).parent('ul').parent('div').siblings('input').val($(this).data('val'));
                    console.log($(this).attr('data-val'));
                });
            }
        });

    }, 1000);


    function init_search_dp(selector, callback) {

        $(selector).after('<div style="position: absolute; background-color: aliceblue; z-index: 900"><ul class="drop-search" style=""></ul></div>');

        $(".drop-search").hide();

        $(selector).keyup(function(e){

            var li = '';
            var ctr = 0;
            var typed = $(this).val();



            var hit = false;

            if(e.keyCode === 40){

                if(start_index_sd < (list_size-1)){
                    start_index_sd++;
                    hit = true;
                }


            }else if(e.keyCode === 38){
                if(start_index_sd > 0){
                    start_index_sd--;
                    hit = true;
                }

            }else if(e.keyCode === 13 || e.keyCode === 27 || typed.length === 0){
                typed = '';
                list_size = 0;
                $(selector).siblings("div").children("ul.drop-search").html('').hide();


                callback();


            }else if(typed.length > 0){

                $.each(list_dp,function(index, value){
                    if(matchPhrase(typed, value)){
                        li += '<li style="" class="list-'+ctr+'" data-val="'+value+'">' + value + ' <img class="" src="' + value + '"></li>'
                        ctr++;
                        list_size = ctr;
                        //alert(2);
                    }
                });

                $(selector).siblings("div").children("ul.drop-search").html(li).show();

            }

            $(selector).siblings("div").children("ul.drop-search").children('li').removeClass('selected');

            if(hit){
                var selected_li_selector = ".list-" + start_index_sd;

                $(selector).siblings("div").children("ul.drop-search").children("li"+selected_li_selector).addClass('selected');

                $(this).val($(selected_li_selector).data("val"));

            }else{
                if(start_index_sd === 0){
                    start_index_sd = list_size;
                }else{
                    start_index_sd = -1;
                }

            }

        });

        $(selector).focusout(function(){
            $(selector).siblings("div").children("ul.drop-search").html('').hide();

            callback();

        });
    }

</script>

