@extends('admin.main')

@section('content')

    <style>


    </style>



    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Contents</h1>
        <div class="btn-toolbar mb-2 mb-md-0"></div>

    </div>

    <div class="card">
        <div class="card-header">
            Create
        </div>
        <div class="card-body">
            <form style="margin-top: 10px" action="{{url('admin/contents')}}" method="post" class="needs-validation" novalidate>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Title</label>
                    <input type="text" class="form-control" name="title" id="exampleFormControlInput1" placeholder="" autocomplete="off" required>


                    <div class="invalid-feedback">
                        Please add a title.
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">Type</label>
                    <select id="inputState" name="content_type_id" class="form-control">
                        @foreach(\App\Model\ContentType::all() as $contentType)
                            <option value="{{ $contentType->id }}" @if($contentType->name == 'Inner') selected="selected" @endif>{{ $contentType->name }}</option>
                        @endforeach

                    </select>
                    <div class="invalid-feedback">
                        Please select a type.
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">Content</label>
                    <textarea class="form-control" id="content" name="content" rows="3" style="" required></textarea>
                    <div class="invalid-feedback">
                        Please add a content.
                    </div>
                </div>

                <div class="float-lg-right" style="margin-bottom: 20px">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="#" id="back-button" class="btn btn-secondary">Back</a>
                </div>

            </form>
        </div>
    </div>

    @include('admin.tiny')

@endsection
