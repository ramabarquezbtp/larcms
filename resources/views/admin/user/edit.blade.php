@extends('admin.main')

@section('content')



    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">{{$mgt_name}}</h1>
        <div class="btn-toolbar mb-2 mb-md-0"></div>

    </div>

    <div class="card">
        <div class="card-header">
            Edit
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form style="margin-top: 10px" action="{{url('admin/' . $controller . '/save-changes/')}}" method="post" class="needs-validation" novalidate>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="{{ $rec->id }}">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Full Name</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="" value="{{ old('name',$rec->name) }}" required>
                    <div class="invalid-feedback">
                        Please add a name.
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" name="email" value="{{ old('email',$rec->email) }}" aria-describedby="emailHelp" placeholder="Enter email" required>
                    <div class="invalid-feedback">Please add a valid email address.</div>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Password</label>
                    <input type="text" class="form-control" id="" name="password" aria-describedby="passwordHelp" placeholder="Password" value="">
                    <div class="invalid-feedback">Please add a password.</div>
                </div>

                <?php
                    $user_states = \App\Model\UserState::all();
                    $selected = [];
                    foreach ($user_states as $row){
                        $selected[$row->id] = '';
                    }

                    $selected[$rec->user_state_id] = 'selected="selected"'
                ?>

                <div class="form-group">
                    <label for="exampleInputEmail1">Status</label>
                    <select class="form-control" name="user_state_id">
                        @foreach($user_states as $row)
                            <option value="{{$row->id}}" {{$selected[$row->id]}}>{{$row->name}}</option>
                        @endforeach
                    </select>
                    <div class="invalid-feedback">Please select a status</div>
                </div>




                <div class="float-lg-right" style="margin-bottom: 20px">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{url("admin/{$controller}")}}" id="" class="btn btn-secondary">Back</a>
                </div>

            </form>
        </div>
    </div>
@endsection
