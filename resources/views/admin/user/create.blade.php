@extends('admin.main')

@section('content')



    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">{{$mgt_name}}</h1>
        <div class="btn-toolbar mb-2 mb-md-0"></div>

    </div>

    <div class="card">
        <div class="card-header">
            Create
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form style="margin-top: 10px" action="{{url('admin/' . $controller)}}" method="post" class="needs-validation" novalidate>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Full Name</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="" value="{{ old('name') }}" required>
                    <div class="invalid-feedback">
                        Please add a name.
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" name="email" value="{{ old('email') }}" aria-describedby="emailHelp" placeholder="Enter email" required>
                    <div class="invalid-feedback">Please add a valid email address.</div>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Password</label>
                    <input type="text" class="form-control" id="" name="password" aria-describedby="passwordHelp" placeholder="Password" value="{{old('password',substr(uniqid(),0,8))}}" required>
                    <div class="invalid-feedback">Please add a password.</div>
                </div>




                <div class="float-lg-right" style="margin-bottom: 20px">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{url("admin/{$controller}")}}" id="" class="btn btn-secondary">Back</a>
                </div>

            </form>
        </div>
    </div>
@endsection
