@extends('admin.main')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Files</h1>
        <div class="btn-toolbar mb-2 mb-md-0"></div>

    </div>

    <div class="card">
        <div class="card-header">
            Upload
            <a class="btn btn-secondary btn-sm float-right" id="" href="{{ url('admin/files') }}"> Back</a>
        </div>
        <div class="card-body">
            <div class="container">
                <div class="row">
                    <div class="col col-md-12">

                        <form action="{{ url('admin/files') }}" class="dropzone">
                            <div class="fallback">
                                <input name="file" type="file" multiple />
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <script src="{{ url('assets/dropzone.js') }}"></script>

@endsection