@extends('admin.main')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Files</h1>
        <div class="btn-toolbar mb-2 mb-md-0"></div>

    </div>

    <nav class="navbar-expand-lg" size="" style="padding: 5px 0">
        <a class="btn btn-primary" href="{{url('admin/files/create')}}">Upload</a>
        @include('admin.search')
    </nav>
    <div style="clear: both"></div>

    <div class="card">
        <div class="card-header">
            List
        </div>
        <div class="card-body">
                <?php $breaker = 6; ?>

                @if(\App\Model\File::count() > 0)
                    @foreach($files as $i => $file)
                        <?php $base64File = (is_file(storage_path('app/' . $file->path)))
                            ?base64_encode(file_get_contents(storage_path('app/' . $file->path)))
                            :base64_encode(file_get_contents(public_path('assets/images/download.png')))?>

                        @if($i == 0 || (($i%$breaker) == 0))
                            {{--<div class="row">--}}
                        @endif
                            <div class="col col-sm-2 float-left" style="margin-bottom: 10px;">
                                <div class="card" style="width: 14rem;">
                                    <div class="gallery-image-wrapper border">
                                        <img class="card-img-top" src="{{ url('files/thumbnail/' . $file->id) }}" alt="Card image cap">
                                    </div>

                                    <div class="card-body file-detail" style="max-height: 108px">

                                        <div class="row">
                                            <div class="col col-sm-12 ">{{ $file->file_name }}</div>
                                        </div>
                                        <div class="row">
                                            <div class="col col-sm-12 ">
                                                <a href="{{ ((in_array($file->mime_type,config('app.image_types')))?url("files/image/{$file->id}"):url("files/doc/{$file->id}"))  }}" target="_blank">
                                                    {{ ((in_array($file->mime_type,config('app.image_types')))?"files/image/{$file->id}":"files/doc/{$file->id}") }}
                                                </a>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col col-sm-5" style="padding-top: 8px">
                                                <a href="{{ url('admin/files/delete/') }}" data-id="{{ $file->id }}" class="delete-this">
                                                    <svg class="feather feather-trash-2 sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="1226"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                                </a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        @if(((($i+1)%$breaker) == 0))
                            {{--</div>--}}
                        @endif
                    @endforeach
                @endif


        </div>

        <div style="padding-left: 20px">
            {{$files->links('vendor.pagination.bootstrap-4')}}
        </div>

    </div>

@endsection