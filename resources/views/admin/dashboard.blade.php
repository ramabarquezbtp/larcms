@extends('admin.main')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Dashboard</h1>
        <div class="btn-toolbar mb-2 mb-md-0"></div>

    </div>

    <style>
        div.dash-row{
            margin: 0;
        }

        .dash-row div.col{
            margin: 0 !important;

        }

        .dash-row div.col-4{
            width: 28% !important;
        }
    </style>

    <div class="row dash-row" style="" id="">
        <div class="col col-8" style="">

            <h2>

                <a href="{{url("admin/users")}}">Users</a>
                <a href="{{url("admin/users/create")}}" class="float-right" style="">
                    <svg class="feather feather-plus-circle sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="971"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                </a>
            </h2>
            <table class="table table-striped">
                <thead>
                <tr class="table-primary">
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col" colspan="2">Email</th>
                </tr>
                </thead>
                <tbody>

                @if(\App\Model\User::count() > 0)
                    @foreach(\App\Model\User::limit(4)->orderBy('created_at','desc')->get() as $row)
                        <tr>
                            <td>{{$row->id}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->email}}</td>
                            <td>
                                <a href="{{url("admin/users/{$row->id}/edit")}}">
                                    <svg class="feather feather-edit-3 sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="496"><path d="M12 20h9"></path><path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path></svg>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>

        <div class="col col-4" style="">

            <h2>

                <a href="{{url("admin/files")}}">Files</a>
                <a href="{{url("admin/files/create")}}" class="float-right" style="">
                    <svg class="feather feather-plus-circle sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="971"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                </a>
            </h2>
            <table class="table table-striped">
                <thead>
                <tr class="table-primary">
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col" colspan="2">Created @</th>
                </tr>
                </thead>
                <tbody>

                @if(\App\Model\File::count() > 0)
                    @foreach(\App\Model\File::limit(4)->orderBy('created_at','desc')->get() as $row)
                        <tr>
                            <td>{{$row->id}}</td>
                            <td>{{$row->file_name}}</td>
                            <td>{{$row->created_at}}</td>
                            <td>

                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>

    </div>

    <div class="row dash-row" style="" id="">

        <div class="col col-4" style="">

            <h2>

                <a href="{{url("admin/contents")}}">Contents</a>
                <a href="{{url("admin/contents/create")}}" class="float-right" style="">
                    <svg class="feather feather-plus-circle sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="971"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                </a>
            </h2>
            <table class="table table-striped">
                <thead>
                <tr class="table-primary">
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col" colspan="2">Created @</th>
                </tr>
                </thead>
                <tbody>

                @if(\App\Model\Content::count() > 0)
                    @foreach(\App\Model\Content::limit(4)->orderBy('created_at','desc')->get() as $row)
                        <tr>
                            <td>{{$row->id}}</td>
                            <td>{{$row->title}}</td>
                            <td>{{$row->created_at}}</td>
                            <td>
                                <a href="{{url("admin/contents/{$row->id}/edit")}}">
                                    <svg class="feather feather-edit-3 sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="496"><path d="M12 20h9"></path><path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path></svg>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>

        </div>
        <div class="col col-8" style="">

            <h2>

                <a href="{{url("admin/pages")}}">Pages</a>
                <a href="{{url("admin/pages/create")}}" class="float-right" style="">
                    <svg class="feather feather-plus-circle sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="971"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                </a>
            </h2>
            <table class="table table-striped">
                <thead>
                <tr class="table-primary">
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col" colspan="2">Created @</th>
                </tr>
                </thead>
                <tbody>

                @if(\App\Model\Page::count() > 0)
                    @foreach(\App\Model\Page::limit(4)->orderBy('created_at','desc')->get() as $row)
                        <tr>
                            <td>{{$row->id}}</td>
                            <td>{{$row->title}}</td>
                            <td><small>{{$row->created_at}}</small></td>
                            <td>
                                <a href="{{url("admin/pages/{$row->id}/edit")}}">
                                    <svg class="feather feather-edit-3 sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="496"><path d="M12 20h9"></path><path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path></svg>
                                </a>
                                <a href="{{url("page{$row->uri}")}}" data-id="11" class="">
                                    <svg data-v-6f720bf2="" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon-svg feather feather-eye"><path data-v-6f720bf2="" d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle data-v-6f720bf2="" cx="12" cy="12" r="3"></circle></svg>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>

    </div>
    <div class="row dash-row" style="" id="">


        <div class="col col-8" style="">

            <h2>

                <a href="{{url("admin/forms")}}">Forms</a>
                <a href="{{url("admin/forms/create")}}" class="float-right" style="">
                    <svg class="feather feather-plus-circle sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="971"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                </a>
            </h2>
            <table class="table table-striped">
                <thead>
                <tr class="table-primary">
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col" colspan="2">Created @</th>
                </tr>
                </thead>
                <tbody>

                @if(\App\Model\Form::count() > 0)
                    @foreach(\App\Model\Form::limit(4)->orderBy('created_at','desc')->get() as $row)
                        <tr>
                            <td>{{$row->id}}</td>
                            <td>{{$row->name}}</td>
                            <td><small>{{$row->created_at}}</small></td>
                            <td>
                                <a href="{{url("admin/forms/{$row->id}/edit")}}">
                                    <svg class="feather feather-edit-3 sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="496"><path d="M12 20h9"></path><path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path></svg>
                                </a>
                                <a href="{{url("admin/forms/{$row->id}")}}" data-id="11" class="">
                                    <svg data-v-6f720bf2="" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon-svg feather feather-eye"><path data-v-6f720bf2="" d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle data-v-6f720bf2="" cx="12" cy="12" r="3"></circle></svg>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>

        <div class="col col-4" style="">

            <?php
            $rex = \App\Model\FormSubmission::limit(4)
                ->orderBy('form_submissions.created_at','desc')
                ->join('forms', 'form_submissions.form_id','=','forms.id')
                ->select(DB::raw('form_submissions.*, forms.name as form_name'))
                ->get();
            //p(json_encode($rex,128))
            ?>

            <h2>

                Form Submission

            </h2>
            <table class="table table-striped">
                <thead>
                <tr class="table-primary">
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col" colspan="2">Submitted @</th>
                </tr>
                </thead>
                <tbody>

                @if(\App\Model\FormSubmission::count() > 0)
                    @foreach($rex as $row)
                        <tr>
                            <td>{{$row->id}}</td>
                            <td>{{$row->form_name}}</td>
                            <td>{{$row->created_at}}</td>
                            <td>
                                <a href="{{url("admin/forms/submissions/view/{$row->id}")}}">
                                    <svg data-v-6f720bf2="" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon-svg feather feather-eye"><path data-v-6f720bf2="" d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle data-v-6f720bf2="" cx="12" cy="12" r="3"></circle></svg>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>



    </div>

    <div class="row dash-row" style="" id="">

        <div class="col col-4" style="">

            <h2>

                <a href="{{url("admin/galleries")}}">Galleries</a>
                <a href="{{url("admin/galleries/create")}}" class="float-right" style="">
                    <svg class="feather feather-plus-circle sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="971"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                </a>
            </h2>
            <table class="table table-striped">
                <thead>
                <tr class="table-primary">
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col" colspan="2">Created @</th>
                </tr>
                </thead>
                <tbody>

                @if(\App\Model\Gallery::count() > 0)
                    @foreach(\App\Model\Gallery::limit(4)->orderBy('created_at','desc')->get() as $row)
                        <tr>
                            <td>{{$row->id}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->created_at}}</td>
                            <td>
                                <a href="{{url("admin/galleries/{$row->id}/edit")}}">
                                    <svg class="feather feather-edit-3 sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="496"><path d="M12 20h9"></path><path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path></svg>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>

        <div class="col col-6" style="">

            <h2>

                <a href="{{url("admin/events")}}">Events</a>
                <a href="{{url("admin/events/create")}}" class="float-right" style="">
                    <svg class="feather feather-plus-circle sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="971"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
                </a>
            </h2>
            <table class="table table-striped">
                <thead>
                <tr class="table-primary">
                    <th scope="col">#</th>
                    <th scope="col">title</th>
                    <th scope="col" colspan="2">Event Date @</th>
                </tr>
                </thead>
                <tbody>

                @if(\App\Model\Gallery::count() > 0)
                    @foreach(\App\Model\Event::limit(4)->orderBy('date','desc')->get() as $row)
                        <tr>
                            <td>{{$row->id}}</td>
                            <td>{{$row->title}}</td>
                            <td>{{$row->date}}</td>
                            <td>
                                <a href="{{url("admin/events/{$row->id}/edit")}}">
                                    <svg class="feather feather-edit-3 sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="496"><path d="M12 20h9"></path><path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path></svg>
                                </a>
                                <a target="_blank" href="{{url("/news-events{$row->uri}")}}" data-id="11" class="">
                                    <svg data-v-6f720bf2="" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon-svg feather feather-eye"><path data-v-6f720bf2="" d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle data-v-6f720bf2="" cx="12" cy="12" r="3"></circle></svg>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>

    </div>


    <div class="row dash-row" style="" id="">



    </div>



@endsection