@extends('admin.main')

@section('content')

    <style>

    </style>

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Events</h1>
        <div class="btn-toolbar mb-2 mb-md-0"></div>
    </div>

    <div class="card">
        <div class="card-header">
            Create
        </div>
        <div class="card-body">
            <form style="margin-top: 10px" action="{{url('admin/' . $controller . '')}}" method="post" class="needs-validation" novalidate>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Title</label>
                    <input type="text" class="form-control" name="title" id="" placeholder="" autocomplete="off" required>
                    <div class="invalid-feedback">
                        Please add a title.
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">URI</label>
                    <input type="text" class="form-control" name="uri" value="/{{uniqid()}}" id="" placeholder="" autocomplete="off" required>
                    <div class="invalid-feedback">
                        Please add a URI.
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">Date</label>
                    <input type="date" class="form-control" name="date" id="" placeholder="" autocomplete="off" required>
                    <div class="invalid-feedback">
                        Please add a Date.
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">Picture Link</label>
                    <input type="text" class="form-control" name="picture_link" id="img-dp" placeholder="" autocomplete="off" required>
                    <div class="image-wrapper" style="display: none; margin-top: 5px"><img src="" style="border: solid 4px gainsboro" /></div>
                    <div class="invalid-feedback">
                        Please add a title.
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">Summary Content</label>
                    <textarea class="form-control" id="summary_content" name="summary_content" rows="3" style="" onkeyup="if(event.keyCode == 13) return false;"></textarea>
                    <div class="invalid-feedback">
                        Please add a content.
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">Full Content</label>
                    <textarea class="form-control" id="content" name="content" rows="3" style="" required></textarea>
                    <div class="invalid-feedback">
                        Please add a content.
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">Publish</label>
                    <input type="checkbox" name="published" class="" value="1" checked="checked">
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">Feature</label>
                    <input type="checkbox" name="featured" class="" value="1">
                </div>

                <div class="float-lg-right" style="margin-bottom: 20px">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="#" id="back-button" class="btn btn-secondary">Back</a>
                </div>

            </form>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            init_search_dp("#img-dp",function(){
                //alert(9);
                if($("#img-dp").val()){

                    $("#img-dp").siblings(".image-wrapper").hide();
                    $("#img-dp").siblings(".image-wrapper").children('img').attr('src',$("#img-dp").val());
                    $("#img-dp").siblings(".image-wrapper").show('slow');
                }
            });
        })

    </script>

    @include('admin.tiny')

@endsection
