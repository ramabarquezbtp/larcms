@extends('admin.main')

@section('content')



    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">{{$mgt_name}}</h1>
        <div class="btn-toolbar mb-2 mb-md-0"></div>

    </div>

    <div class="card">
        <div class="card-header">
            Edit
        </div>
        <div class="card-body">
            <form style="margin-top: 10px" action="{{url('admin/'.$controller.'/save-changes')}}" method="post" class="needs-validation" novalidate>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="{{ $rec->id }}">
                <input type="hidden" name="contents" value="{{ implode(",",$contents) }}">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Title</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="" value="{{$rec->name}}" required>
                    <div class="invalid-feedback">
                        Please add a Name.
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        <input class="btn btn-info" type="button" value="(+) Add Photos From Media Folder" data-toggle="modal" data-target="#addContentModal">
                    </label>

                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">Content</label>
                    <div class="" style="max-height: 480px; overflow-x:hidden ; overflow-y: scroll">
                        <div class="row text-center text-lg-left" id="gallery-content">
                            {!! html_entity_decode($content_images) !!}
                        </div>
                    </div>
                </div>

                <div class="float-lg-right" style="margin-bottom: 20px">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="#" id="back-button" class="btn btn-secondary">Back</a>
                </div>

            </form>
        </div>
    </div>
    @include('admin.' . $view_folder . '.selectContent')

    <script type="text/javascript">
        $(document).ready(function(){
            @foreach($rec->gallery_photo as $row)
                $('a[data-id="{{$row->id}}"] img').addClass("select-this-image");
            @endforeach
        });
    </script>

@endsection
