@extends('admin.main')

@section('content')



    <div class="d-flex justify-form-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">{{$mgt_name}}</h1>
        <div class="btn-toolbar mb-2 mb-md-0"></div>
    </div>

    <div class="card">
        <div class="card-header">
            View

            <a href="{{url("admin/galleries/{$rec->id}/edit")}}">
                <svg data-v-6f720bf2="" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon-svg feather feather-edit-3"><path data-v-6f720bf2="" d="M12 20h9"></path><path data-v-6f720bf2="" d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path></svg>
            </a>

            <input type="button" class="btn btn-sm btn-success float-right" value="Copy Gallery Outer HTML" id="copyCat">

        </div>
        <div class="card-body">
            @include('admin.gallery.images')
        </div>

    </div>

    <textarea id="myInput" class="form-control"></textarea>

    <script type="text/javascript">

        $("#myInput").val($(".card-body").html());

        $("#copyCat").on('click',function(){
            copyCat();
            $(this).val("HTML Copied").attr("disabled","disabled");
        });

        function copyCat() {
            /* Get the text field */
            //var copyText = $(".card-body").html();

            var copyText = document.getElementById("myInput");

            /* Select the text field */
            copyText.select();

            /* Copy the text inside the text field */
            document.execCommand("copy");

            /* Alert the copied text */
            //alert("Copied the text: " + copyText.value);
        }
    </script>





@endsection
