<!-- Modal -->
<style>
    .modal-lg{
        width: 60% !important;
        max-width: 100% !important;
    }
    .select-this-image{
        border: solid #007bff 10px;
    }
</style>
<div class="modal fade" id="addContentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Select Images</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>



            <div class="modal-body">

                <!-- Page Content -->
                <div class="container" style="max-height: 500px; overflow-y: scroll">


                    <div class="row text-center text-lg-left">

                        @foreach($photos as $row)

                            <div class="col-lg-3 col-md-4 col-6">
                                <a href="#" class="d-block mb-4 h-100 image-selection" data-id="{{$row->id}}">
                                    <img class="img-fluid img-thumbnail" src="{{ url('files/thumbnail/' . $row->id) }}" alt="">
                                </a>
                            </div>

                        @endforeach

                    </div>


                </div>
                <!-- /.container -->


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="close-gallery-selection" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="add-gallery-contents">Save changes</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var image_collection = {};
    $(document).ready(function(){
        $(".image-selection").on('click',function(){

            var value = $(this).data("id");
            var index = "id-" + value;

            if(index in image_collection){
                delete image_collection[index];
                $(this).children('img').removeClass("select-this-image");
            }else{
                image_collection[index] = value;
                $(this).children('img').addClass("select-this-image");
            }

        });

        $("#add-gallery-contents").on('click',function(){
            $("#gallery-content").html('');
            var contents = [];
            $.each(image_collection,function(i, v){
                var image = '<div class="col-lg-3 col-md-4 col-6">\n' +
                    '                                <a href="#" class="d-block mb-4 h-100">\n' +
                    '                                    <img class="img-fluid img-thumbnail" src="'+base_url+'/files/thumbnail/'+v+'" alt="">\n' +
                    '                                </a>\n' +
                    '                            </div>';
                $("#gallery-content").append(image);
                contents.push(v);
            });

            //$("#addContentModal").modal('hide');
            //$(".modal-backdrop").hide();
            //alert(JSON.stringify(contents));

            if(contents.length > 0){
                $('input[name="contents"]').val(contents.join(','));
            }else{
                $('input[name="contents"]').val('');
            }



            $("[data-dismiss=modal]").trigger({ type: "click" });

        });

    });
</script>