<div class="demo-gallery">
    <ul id="lightgallery" class="gallery-wrapper row">
        {!! html_entity_decode($gallery['items']) !!}
    </ul>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#lightgallery').lightGallery();
    });
</script>

<div class="col-lg-6"></div>