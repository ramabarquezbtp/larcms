@extends('admin.main')

@section('content')

    <div class="d-flex justify-form-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">forms</h1>
        <div class="btn-toolbar mb-2 mb-md-0"></div>
    </div>

    <div class="card">
        <div class="card-header">
            Edit
        </div>
        <div class="card-body">
            <form style="margin-top: 10px" action="{{url('admin/forms/save-changes')}}" method="post" class="needs-validation" novalidate>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="{{ $form->id }}">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Name</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="" value="{{ $form->name }}" required>
                    <div class="invalid-feedback">
                        Please add a name.
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">form</label>
                    <textarea class="form-control" id="content" name="content" rows="10" required>{{ $form->content }}</textarea>
                    <div class="invalid-feedback">
                        Please add a content.
                    </div>
                </div>
                <div class="float-lg-right" style="margin-bottom: 20px">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="#" id="back-button" class="btn btn-secondary">Back</a>
                </div>
            </form>
        </div>
    </div>

@endsection