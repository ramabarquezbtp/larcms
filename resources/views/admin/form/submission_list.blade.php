@extends('admin.main')

@section('content')

    <div class="d-flex justify-form-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">{{\App\Model\Form::find($form_id)->name}} Form Entries</h1>
        <div class="btn-toolbar mb-2 mb-md-0"></div>

    </div>

    <nav class="navbar-expand-lg" size="" style="padding: 5px 0">
        <a href="#" id="back-button" class="btn btn-secondary">Back</a>

    </nav>

    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>

                <th></th>
                <th>Form Name</th>
                <th>Date Created</th>
                <th style="text-align: center !important;">Action</th>
            </tr>
            </thead>
            <tbody>
            @if(\App\Model\FormSubmission::count() > 0)
                @foreach($data as $row)
                    <tr>
                        <td>Submission #: {{$row->id}}</td>
                        <td>
                            {{$row->form()->name}}
                        </td>
                        <td>{{$row->created_at}}</td>

                        <td align="center">
                            <a href="{{ url('admin/forms/submissions/view/' . $row->id)  }}" data-id="{{ $row->id }}" class="">
                                <svg data-v-6f720bf2="" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon-svg feather feather-eye"><path data-v-6f720bf2="" d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle data-v-6f720bf2="" cx="12" cy="12" r="3"></circle></svg>
                            </a>

                        </td>
                    </tr>
                @endforeach
            @endif

            </tbody>


        </table>
    </div>

    <div>

    </div>






@endsection