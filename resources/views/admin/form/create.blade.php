@extends('admin.main')

@section('content')



    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Form</h1>
        <div class="btn-toolbar mb-2 mb-md-0"></div>

    </div>

    <div class="card">
        <div class="card-header">
            Create
        </div>
        <div class="card-body">
            <form style="margin-top: 10px" action="{{url('admin/forms')}}" method="post" class="needs-validation" novalidate>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Name</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="" required>
                    <div class="invalid-feedback">
                        Please add a name.
                    </div>
                </div>



                <div class="form-group">
                    <label for="exampleFormControlInput1">Content</label>
                    <textarea class="form-control" id="content" name="content" rows="10" style="" required>{
    "name": "resume",
    "action": "\/admin\/forms",
    "method": "post",
    "tabs": [
        [
            [
                {
                    "name": "Youth Camp fee is $250 per week and can be paid at our main office at: 212 Bysham Park Drive Woodstock, Ontario N4T 1R2 or by phone at 519-539-7447",
                    "type": "checkbox",
                    "values": [
                        "I will call to provide payment No",
                        "I will come to the main office to provide payment"
                    ]
                }
            ],
            [
                {
                    "name": "First Name",
                    "type": "text",
                    "validation": ['required']
                }
            ]
        ]
    ]
}
                    </textarea>
                    <div class="invalid-feedback">
                        Please add a content.
                    </div>
                </div>

                <div class="float-lg-right" style="margin-bottom: 20px">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="#" id="back-button" class="btn btn-secondary">Back</a>
                </div>

            </form>
        </div>
    </div>
@endsection
