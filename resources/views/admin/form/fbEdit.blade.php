@extends('admin.main')

@section('content')

    <style>
        .fb-main {
            background-color: #fff;
            border-radius: 5px;
            min-height: 600px;
        }
    </style>


    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Form</h1>
        <div class="btn-toolbar mb-2 mb-md-0"></div>

    </div>

    <div class="card">
        <div class="card-header">
            Edit
        </div>
        <div class="card-body">

            <form style="margin-top: 10px" action="{{url('admin/forms/save-changes')}}" method="post" class="needs-validation" id="form-form" novalidate>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="form_id" value="{{ $id }}">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Name</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="" value="{{$form_name}}" required>
                    <div class="invalid-feedback">
                        Please add a name.
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">
                        <input type="button" class="btn btn-sm btn-info" id="add-form-page-edit" value="Add Form Page">
                        <a href="{{url('admin/forms/remove-form/'.$current_page.'/'.$id)}}" class="btn btn-sm btn-danger">Remove current form</a>
                        <input type="submit" class="btn btn-sm btn-primary" id="save-form-page" value="Save">
                        <a href="{{url("admin/{$controller}")}}" class="btn btn-sm btn-secondary">Back</a>
                    </label>

                </div>

            </form>

            <input type="hidden" name="page-count" id="page-count" value="1">
            <input type="hidden" name="current-page" id="current-page" value="{{$current_page}}">
            <input type="hidden" name="page_count" id="page_count" value="{{$page_count}}">
            <div class="btn-wrapper">
                <ul class="nav nav-tabs">


                    @for($i = 1; $i <= $page_count; $i++)
                        <?php $tab_state[$i] = ''?>
                        <?php $tab_state[$current_page] = 'active'?>
                        <li class="nav-item">
                            <a class="page-tab nav-link {{$tab_state[$i]}}" href="{{ url('admin/forms/'.$id.'/edit/'.$i) }}">Page {{$i}}</a>
                        </li>
                        {{--<a href="{{ url('admin/forms/create/'.$i) }}" class="btn btn-{{$tab_state[$i]}} page-tab">Page {{$i}}</a>--}}
                    @endfor

                </ul>

            </div>

            <div class='fb-main'></div>

            <script src="{{ url('assets/formbuilder/vendor/js/vendor.js') }}"></script>
            <script src="{{ url('assets/formbuilder/dist/formbuilder.js') }}"></script>
            @include('admin/form/common')


        </div>
    </div>
@endsection
