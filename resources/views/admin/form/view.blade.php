@extends('admin.main')

@section('content')

    <div class="d-flex justify-form-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">forms</h1>
        <div class="btn-toolbar mb-2 mb-md-0"></div>
    </div>

    <div class="card">
        <div class="card-header">
            View
            <a href="{{url("admin/forms/{$form_id}/edit")}}" class="float-right">
                <svg class="feather feather-edit-3 sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="496"><path d="M12 20h9"></path><path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path></svg>
            </a>
        </div>
        <div class="card-body">
            {!! html_entity_decode($form) !!}
        </div>
    </div>

    <script type="text/javascript" src="/assets/theme1/admin.form.js"></script>
    @include('reCaptcha')

@endsection
