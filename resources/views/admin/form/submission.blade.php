@extends('admin.main')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Form Submission</h1>
        <div class="btn-toolbar mb-2 mb-md-0"></div>

    </div>
    <div class="row">
        <div class="col" style="margin-bottom: 5px">
            <a href="#" id="back-button" class="btn btn-secondary">Back</a>
        </div>

    </div>
    <div class="card">
        <div class="card-header">
            {{$data->form()->name}} Form Entry

        </div>
        <div class="card-body">
            <ul style="list-style-type: none">
                @foreach(json_decode($data->content) as $i => $value)
                    @if(!in_array($i,['formName','tab-count','form_id','token','g-recaptcha-response']))
                        <li>
                            @if(strpos($i,"Header_h3") !== false)
                                <h3>{{ preg_replace(['/\|[0-9]/'],[""], str_replace("_"," ",$value)) }}</h3>
                            @else
                                <span style="font-weight: bold">{{ preg_replace(['/\|[0-9]/','/\*/','/:/','/\:/'],["","","",""], str_replace("_"," ",$i)) }}</span>
                            @endif


                            @if(strpos($i,"Header_h3") === false)

                                    <ul>
                                        @if(is_object($value) || is_array($value))

                                            @foreach($value as $ii => $vv)
                                                <li>{{$vv}}</li>
                                            @endforeach

                                        @elseif(is_object($value))
                                            @if(isset($value->size))
                                                <form action="{{url('download_file')}}" method="post">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <input style="display: none" name="mime-type" type="text" value="{{$value->type}}">
                                                    <input style="display: none" name="size" type="text" value="{{$value->size}}">
                                                    <input style="display: none" name="name" type="text" value="{{$value->name}}">
                                                    <textarea style="display: none" name="content">{{$value->tmp_name}}</textarea>
                                                    <input type="submit" value="Download">
                                                </form>
                                            @endif
                                        @else


                                                <li>
                                                    @if(strpos($value,';base64,'))
                                                        <form action="{{url('download_file')}}" method="post">
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                                            <textarea style="display: none" name="content">{{$value}}</textarea>
                                                            <input type="submit" value="Download">
                                                        </form>
                                                    @else
                                                        {{$value}}
                                                    @endif
                                                </li>



                                        @endif
                                    </ul>

                            @endif

                        </li>
                    @endif

                @endforeach
            </ul>
        </div>
    </div>
@endsection
