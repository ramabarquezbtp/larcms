<script>

    var form = {!! html_entity_decode(json_encode($form)) !!};

    var fb = [];

    $(function(){
        fb = new Formbuilder({
            selector: '.fb-main',
            bootstrapData: form
        });

        fb.on('save', function(payload){
            console.log(payload);
            saveSession(payload);
        })
    });

    $(".page-tab").on('click',function(e){
    });

    function saveSession(payload) {
        //alert('888');
        var post = {
            '_token':'{{ csrf_token() }}',
            'payload':payload,
            'form_id':$('input[name="form_id"]').val(),
            'form_name':$('input[name="name"]').val(),
            'form_page_index': $("#current-page").val()
        };
        $.post('{{ url('admin/forms/temp-store') }}',post,function(e){
            //alert(e);
        });
    }

    $("#add-form-page").on('click',function(){
        var page_count = parseInt($('#page_count').val());
        window.location = '{{ url('admin/forms/add-page') }}/' + (page_count+1);
    });

    $("#add-form-page-edit").on('click',function(){
        var page_count = parseInt($('#page_count').val());
        var id = $('input[name="form_id"]').val();
        //alert(id);
        window.location = '{{ url('admin/forms/add-page') }}/' + (page_count+1) + '/' + id;
    });

    $(document).ready(function(){
        $("#save-form-page").click(function (e) {
            e.preventDefault();
            //alert('lo');
            $(".js-save-form").click();
            $("#form-form").submit();
        });
    });

</script>