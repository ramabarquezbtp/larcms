<!-- Modal -->
<div class="modal fade" id="addContentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Select Contents</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <?php
            $checked = 'checked="checked"';
            $headerS = $checked;
            $footerS = $checked;

            if(isset($selected_contents) && is_array($selected_contents)){
                $headerS = '';
                $footerS = '';
            }else{
                $selected_contents = [];
                $selected_forms = [];
                $selected_galleries = [];
            }
            ?>

            <div class="modal-body" style="max-height: 500px; overflow-y: scroll">

                <ul>
                    <li>
                        Header
                        <ul>
                            @if(count($contents) > 0 && isset($contents['Header']))
                                @foreach($contents['Header'] as $header)
                                    <li>
                                        <div class="">
                                            <input class="form-check-input" type="checkbox" {{$headerS}} name="content[]" id="" {{ (in_array($header->id,$selected_contents))?$checked:"" }} value="{{$header->id}}" data-contenttype="Header" data-title="{{$header->title}}">
                                            <label class="custom-control-label" for="customCheck1">{{ $header->title }}</label>
                                        </div>
                                    </li>
                                    <?php $headerS = ''?>
                                @endforeach
                            @endif

                        </ul>
                    </li>
                    <li>
                        Footer
                        <ul>
                            @if(count($contents) > 0 && isset($contents['Footer']))
                                @foreach($contents['Footer'] as $header)
                                    <li>
                                        <div class="">
                                            <input class="form-check-input" type="checkbox" {{$footerS}} name="content[]" id="" {{ (in_array($header->id,$selected_contents))?$checked:"" }} value="{{$header->id}}" data-contenttype="Footer" data-title="{{$header->title}}">
                                            <label class="custom-control-label" for="customCheck1">{{ $header->title }}</label>
                                        </div>
                                    </li>
                                    <?php $footerS = ''?>
                                @endforeach
                            @endif

                        </ul>
                    </li>
                    <li>
                        Inner Content
                        <ul>
                            @if(count($contents) > 0 && isset($contents['Inner']))
                                @foreach($contents['Inner'] as $header)
                                    <li>
                                        <div class="">
                                            <input class="form-check-input" type="checkbox" name="content[]" id="" {{ (in_array($header->id,$selected_contents))?$checked:"" }} value="{{$header->id}}" data-contenttype="Inner" data-title="{{$header->title}}">
                                            <label class="custom-control-label" for="customCheck1">{{ $header->title }}</label>
                                        </div>
                                    </li>
                                @endforeach
                            @endif

                        </ul>
                    </li>
                    <li>
                        Forms
                        <ul>
                            @if($forms)
                                @foreach($forms as $form)
                                    <li>
                                        <div class="">
                                            <input class="form-check-input" type="checkbox" name="form[]" id="" {{ (in_array($form->id,$selected_forms))?$checked:"" }} value="{{$form->id}}" data-contenttype="Form" data-title="{{$form->name}}">
                                            <label class="custom-control-label" for="customCheck1">{{ $form->name }}</label>
                                        </div>
                                    </li>
                                @endforeach
                            @endif

                        </ul>
                    </li>

                    <li>
                        Galleries
                        <ul>
                            @if($galleries)
                                @foreach($galleries as $gallery)
                                    <li>
                                        <div class="">
                                            <input class="form-check-input" type="checkbox" name="gallery[]" id="" {{ (in_array($gallery->id,$selected_galleries))?$checked:"" }} value="{{$gallery->id}}" data-contenttype="Gallery" data-title="{{$gallery->name}}">
                                            <label class="custom-control-label" for="customCheck1">{{ $gallery->name }}</label>
                                        </div>
                                    </li>
                                @endforeach
                            @endif

                        </ul>
                    </li>
                </ul>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="add-contents">Save changes</button>
            </div>
        </div>
    </div>
</div>