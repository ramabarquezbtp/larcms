@extends('admin.main')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Pages</h1>
        <div class="btn-toolbar mb-2 mb-md-0"></div>

    </div>

    <div class="card">
        <div class="card-header">
            Create
        </div>
        <div class="card-body">
            <form style="margin-top: 10px" action="{{url('admin/pages')}}" method="post" class="needs-validation" novalidate>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Title</label>
                    <input type="text" class="form-control" name="title" id="exampleFormControlInput1" placeholder="" required>
                    <div class="invalid-feedback">
                        Please add a title.
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">URI</label>
                    <input type="text" class="form-control" name="uri" id="exampleFormControlInput2" placeholder="" value="/{{uniqid()}}" required>
                    <div class="invalid-feedback">
                        Please add a URI.
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">Head</label>
                    <textarea class="form-control" name="head"></textarea>
                </div>

                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="page_state_id" value="1" checked="checked" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">
                        Publish
                    </label>
                </div>

                <div class="form-check" style="">
                    <input class="form-check-input" type="checkbox" name="private" value="1" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">
                        Is Private
                    </label>
                </div>

                <div class="form-group" style="margin-top: 20px">
                    {{--<label for="exampleFormControlInput1">Contents</label>
                    <input class="btn btn-info" type="button" value="Add/Remove Page Content" data-toggle="modal" data-target="#addContentModal">--}}

                    <div class="row" style="margin-bottom: 10px">
                        <div class="col">
                            <label for="exampleFormControlInput1">Contents</label>
                            <input class="btn btn-info" type="button" value="Add/Remove Page Content" data-toggle="modal" data-target="#addContentModal">
                        </div>
                    </div>


                    {{--<div class="card" style="width: 650px; margin-top: 15px">
                        <ul class="list-group list-group-flush" id="selected-content">
                        </ul>
                    </div>--}}

                    <div class="row">
                        <div class="card" style="/*width: 850px; margin-top: 15px*/" class="col col-5">
                            <ul class="list-group list-group-flush" id="selected-content">

                            </ul>
                        </div>
                    </div>


                </div>


                @include('admin.page.selectContent')



                <div class="float-lg-right" style="margin-bottom: 20px">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="#" id="back-button" class="btn btn-secondary">Back</a>
                </div>
            </form>
        </div>
    </div>

@endsection