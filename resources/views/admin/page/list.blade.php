@extends('admin.main')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Pages</h1>
        <div class="btn-toolbar mb-2 mb-md-0"></div>

    </div>

    <nav class="navbar-expand-lg" size="" style="padding: 5px 0">
        <a class="btn btn-primary" href="{{url('admin/pages/create')}}">Create</a>

        @include('admin.search')

    </nav>

    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>

                {!! html_entity_decode($header) !!}
                <th style="text-align: center !important;">Action</th>
            </tr>
            </thead>
            <tbody>
            @if(\App\Model\Page::count() > 0)
                @foreach($rec as $row)
                    <tr>
                        <td>{{$row->title}}</td>
                        <td>{{$row->uri}}</td>
                        <td>{{$row->user->name}}</td>
                        <td>{{$row->page_state->name}}</td>
                        <td>{{$row->created_at}}</td>
                        <td>{{$row->updated_at}}</td>
                        <td align="center">
                            <a href="{{ route('pages.edit',$row->id)}}">
                                <svg class="feather feather-edit sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="501"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg>
                            </a>
                            <a href="{{ url('admin/pages/delete')  }}" data-id="{{ $row->id }}" class="delete-this">
                                <svg class="feather feather-delete sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="461"><path d="M21 4H8l-7 8 7 8h13a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2z"></path><line x1="18" y1="9" x2="12" y2="15"></line><line x1="12" y1="9" x2="18" y2="15"></line></svg>
                            </a>

                            <a href="/{{ (($row->private)?"portal":"page").$row->uri  }}" data-id="{{ $row->id }}" class="view-this" target="_blank">
                                <svg data-v-6f720bf2="" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon-svg feather feather-eye"><path data-v-6f720bf2="" d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle data-v-6f720bf2="" cx="12" cy="12" r="3"></circle></svg>
                            </a>

                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>


        </table>
    </div>

    <div>
        {{$rec->links('vendor.pagination.bootstrap-4')}}
    </div>

@endsection