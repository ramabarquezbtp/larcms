@extends('admin.main')

@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1 class="h2">Pages</h1>
        <div class="btn-toolbar mb-2 mb-md-0"></div>

    </div>

    <div class="card">
        <div class="card-header">
            Edit
        </div>
        <div class="card-body">
            <form style="margin-top: 10px" action="{{url('admin/pages/save-changes')}}" method="post" class="needs-validation" novalidate>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="{{ $page->id }}">
                <div class="form-group">
                    <label for="exampleFormControlInput1">Title</label>
                    <input type="text" class="form-control" name="title" id="exampleFormControlInput1" placeholder="" value="{{$page->title}}" required>
                    <div class="invalid-feedback">
                        Please add a title.
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">URI</label>
                    <input type="text" class="form-control" name="uri" id="exampleFormControlInput2" placeholder="" value="{{$page->uri}}" required>
                    <div class="invalid-feedback">
                        Please add a title.
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleFormControlInput1">Head</label>
                    <textarea class="form-control" name="head">{{ $page->head }}</textarea>
                </div>

                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="page_state_id" @if($page->page_state_id == 1) checked="checked" @endif value="1" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">
                        Published
                    </label>
                </div>

                <div class="form-check" style="">
                    <input class="form-check-input" type="checkbox" name="private" @if($page->private == 1) checked="checked" @endif value="1" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">
                        Is Private
                    </label>
                </div>

                <div class="form-group" style="margin-top: 20px">
                    <div class="row" style="margin-bottom: 10px">
                        <div class="col">
                            <label for="exampleFormControlInput1">Contents</label>
                            <input class="btn btn-info" type="button" value="Add/Remove Page Content" data-toggle="modal" data-target="#addContentModal">
                        </div>
                    </div>


                    <div class="row">
                        <div class="card" style="/*width: 850px; margin-top: 15px*/" class="col col-5">
                            <ul class="list-group list-group-flush" id="selected-content">
                                @if($page->page_content)
                                    @foreach($page->page_content as $v)
                                        <?php

                                        if($v->form_id){
                                            $type = 'Form';
                                            $title = $v->form->name;
                                            $content_id = $v->form_id;
                                            $order = $v->order;
                                            $input_name = 'form_id';
                                            $edit_link = url("admin/forms/{$v->form_id}/edit");
                                        }elseif($v->gallery_id){
                                            $type = 'Gallery';
                                            $title = $v->gallery->name;
                                            $content_id = $v->gallery_id;
                                            $order = $v->order;
                                            $input_name = 'gallery_id';
                                            $edit_link = url("admin/galleries/{$v->gallery_id}/edit");
                                        }else{
                                            $type = $v->content->content_type->name;
                                            $title = $v->content->title;
                                            $content_id = $v->content_id;
                                            $order = (!in_array($v->content->content_type->name,['Footer','Header']))?$v->order:"";
                                            $input_name = 'content_id';
                                            $edit_link = url("admin/contents/{$v->content_id}/edit");
                                        }

                                        ?>
                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col col-lg-10">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="width: 79px">{{ $type }}</span>
                                                            <span class="input-group-text" style="width: 473px">{{ $title }}</span>
                                                        </div>
                                                        <input type="hidden" name="{{$input_name}}[]" value="{{ $content_id }}">
                                                        <input type="number" name="content_order[]" class="form-control" value="{{ $order }}" aria-label="" placeholder="">

                                                    </div>
                                                </div>
                                                <div class="col col-lg-1">
                                                    @if(!in_array($type,['Header','Footer']))
                                                        <a href="{{$edit_link}}" target="_blank" class="float-left">
                                                            <svg class="feather feather-edit-3 sc-dnqmqq jxshSx" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" aria-hidden="true" data-reactid="496"><path d="M12 20h9"></path><path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path></svg>
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>


                @include('admin.page.selectContent')

                <div class="float-lg-right" style="margin-bottom: 20px">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="#" id="back-button" class="btn btn-secondary">Back</a>
                </div>

            </form>
        </div>
    </div>

@endsection