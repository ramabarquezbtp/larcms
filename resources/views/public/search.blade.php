<div class="container">
    <section class="error-404 not-found focusable" tabindex="-1">
        <header class="page-header" style="color:#8a8d36">
            <h1 class="page-title">Search Results for: {{$keyword}}</h1>
        </header>
        <!-- .page-header -->
        <div class="page-content">

            @if($result)
                @foreach($result as $row)
                    @if($row['title'] != 'Site Map')
                        <brand id="post-8" class="post-8 page type-page status-publish hentry">

                            <header class="entry-header">
                                <h2 class="entry-title" style="color:#8a8d36"><a href="{{url("page{$row['uri']}")}}" rel="bookmark" style="color:#8a8d36">{{$row['title']}}</a></h2>	</header><!-- .entry-header -->

                            <div class="entry-summary">
                                <p>{{ substr($row['p'],0,1000) }}
                                    <a href="{{url("page{$row['uri']}")}}" class="more-link" style="color:#8a8d36">Continue reading <span class="screen-reader-text">{{$row['title']}}</span></a></p>
                            </div><!-- .entry-summary -->

                        </brand><!-- #post-## -->

                    @endif
                @endforeach
            @else

                <p>It looks like nothing was found. Maybe try a searching again?</p>
                <form role="search" method="get" class="search-form" action="/page/home">
                    <label>
                        <span class="screen-reader-text">Search for:</span>
                        <input type="search" class="search-field" placeholder="Search …" value="" name="s">
                    </label>
                    <input type="submit" class="search-submit screen-reader-text" value="Search">
                </form>

            @endif
        </div>
        <!-- .page-content -->
    </section>
</div>