<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/assets/theme1/wp-content/themes/wddswp/favicon.png">
    <title>
        WDDS
    </title>
    <link href="/assets/theme1/wp-content/themes/wddswp/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/theme1/wp-content/themes/wddswp/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <script src="/assets/theme1/wp-content/themes/wddswp/js/jquery-1.12.1.min.js" ></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Adamina" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli:400,600,700,900" rel="stylesheet">
    <script src="/assets/theme1/wp-content/themes/wddswp/js/jquery-ui.min.js" ></script>
    <script src="https://f1-na.readspeaker.com/script/7304/ReadSpeaker.js?pids=embhl" type="text/javascript"></script>
    <script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
    <title>WDDS &#8211; A community where everybody belongs</title>
    <style type='text/css'>
        .a11y-toolbar ul li button {
            font-size: 20px !important;
        }
    </style>
    <script type="text/javascript">function heateorSssLoadEvent(e) {var t=window.onload;if (typeof window.onload!="function") {window.onload=e}else{window.onload=function() {t();e()}}};	var heateorSssSharingAjaxUrl = '/assets/theme1/wp-admin/admin-ajax.php', heateorSssCloseIconPath = '/assets/theme1/wp-content/plugins/sassy-social-share/public/../images/close.png', heateorSssPluginIconPath = '/assets/theme1/wp-content/plugins/sassy-social-share/public/../images/logo.png', heateorSssHorizontalSharingCountEnable = 0, heateorSssVerticalSharingCountEnable = 0, heateorSssSharingOffset = -10; var heateorSssMobileStickySharingEnabled = 0;var heateorSssCopyLinkMessage = "Link copied.";var heateorSssUrlCountFetched = [], heateorSssSharesText = 'Shares', heateorSssShareText = 'Share';function heateorSssPopup(e) {window.open(e,"popUpWindow","height=400,width=600,left=400,top=100,resizable,scrollbars,toolbar=0,personalbar=0,menubar=no,location=no,directories=no,status")}</script>
    <style type="text/css">
        .heateor_sss_horizontal_sharing .heateorSssSharing{
            color: #fff;
            border-width: 0px;
            border-style: solid;
            border-color: transparent;
        }
        .heateor_sss_horizontal_sharing .heateorSssTCBackground{
            color:#666;
        }
        .heateor_sss_horizontal_sharing .heateorSssSharing:hover{
            border-color: transparent;
        }
        .heateor_sss_vertical_sharing .heateorSssSharing{
            color: #fff;
            border-width: 0px;
            border-style: solid;
            border-color: transparent;
        }
        .heateor_sss_vertical_sharing .heateorSssTCBackground{
            color:#666;
        }
        .heateor_sss_vertical_sharing .heateorSssSharing:hover{
            border-color: transparent;
        }
        @media screen and (max-width:783px) {.heateor_sss_vertical_sharing{display:none!important}}
    </style>
    <meta name='robots' content='noindex,follow' />
    <link rel='dns-prefetch' href='index.html' />
    <link rel='dns-prefetch' href='https://fonts.googleapis.com/' />
    <link rel='dns-prefetch' href='https://secure.rating-widget.com/' />
    <link rel='dns-prefetch' href='https://s.w.org/' />
    <link rel="alternate" type="application/rss+xml" title="WDDS &raquo; Feed" href="feed/index.html" />
    <link rel="alternate" type="application/rss+xml" title="WDDS &raquo; Comments Feed" href="comments/feed/index.html" />
    <link rel="alternate" type="text/calendar" title="WDDS &raquo; iCal Feed" href="events/index.html%3Fical=1" />
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.wdds.ca\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.8"}};
        !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='slickmap.css-css'  href='/assets/theme1/wp-content/plugins/slick-sitemap/slickmap.css%3Fver=4.9.8.css' type='text/css' media='all' />
    <link rel='stylesheet' id='ai1ec_style-css'  href='/assets/theme1/wp-content/plugins/all-in-one-event-calendar/public/themes-ai1ec/vortex/css/ai1ec_parsed_css.css%3Fver=2.5.36.css' type='text/css' media='all' />
    <link rel='stylesheet' id='bwg_sumoselect-css'  href='/assets/theme1/wp-content/plugins/photo-gallery/css/sumoselect.min.css%3Fver=3.0.3.css' type='text/css' media='all' />
    <link rel='stylesheet' id='bwg_font-awesome-css'  href='/assets/theme1/wp-content/plugins/photo-gallery/css/font-awesome/font-awesome.min.css%3Fver=4.6.3.css' type='text/css' media='all' />
    <link rel='stylesheet' id='bwg_mCustomScrollbar-css'  href='/assets/theme1/wp-content/plugins/photo-gallery/css/jquery.mCustomScrollbar.min.css%3Fver=1.5.13.css' type='text/css' media='all' />
    <link rel='stylesheet' id='dashicons-css'  href='/assets/theme1/wp-includes/css/dashicons.min.css%3Fver=4.9.8.css' type='text/css' media='all' />
    <link rel='stylesheet' id='bwg_googlefonts-css'  href='https://fonts.googleapis.com/css?family=Ubuntu&#038;subset=greek,latin,greek-ext,vietnamese,cyrillic-ext,latin-ext,cyrillic' type='text/css' media='all' />
    <link rel='stylesheet' id='bwg_frontend-css'  href='/assets/theme1/wp-content/plugins/photo-gallery/css/bwg_frontend.css%3Fver=1.5.13.css' type='text/css' media='all' />
    <link rel='stylesheet' id='rw-site-rating-css'  href='/assets/theme1/wp-content/plugins/rating-widget/resources/css/site-rating.css%3Fver=3.0.3.css' type='text/css' media='all' />
    <link rel='stylesheet' id='UserAccessManagerLoginForm-css'  href='/assets/theme1/wp-content/plugins/user-access-manager/assets/css/uamLoginForm.css%3Fver=2.1.11.css' type='text/css' media='screen' />
    <link rel='stylesheet' id='wpa-style-css'  href='/assets/theme1/wp-content/plugins/wp-accessibility/css/wpa-style.css%3Fver=4.9.8.css' type='text/css' media='all' />
    <link rel='stylesheet' id='ui-font.css-css'  href='/assets/theme1/wp-content/plugins/wp-accessibility/toolbar/fonts/css/a11y-toolbar.css%3Fver=4.9.8.css' type='text/css' media='all' />
    <link rel='stylesheet' id='ui-a11y.css-css'  href='/assets/theme1/wp-content/plugins/wp-accessibility/toolbar/css/a11y.css%3Fver=4.9.8.css' type='text/css' media='all' />
    <link rel='stylesheet' id='ui-fontsize.css-css'  href='/assets/theme1/wp-content/plugins/wp-accessibility/toolbar/css/a11y-fontsize.css%3Fver=4.9.8.css' type='text/css' media='all' />
    <link rel='stylesheet' id='cssblog-css'  href='/assets/theme1/wp-content/plugins/wp-blog-and-widgets/css/styleblog.css%3Fver=1.6.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wddswp-fonts-css'  href='https://fonts.googleapis.com/css?family=Noto+Sans%3A400italic%2C700italic%2C400%2C700%7CNoto+Serif%3A400italic%2C700italic%2C400%2C700%7CInconsolata%3A400%2C700&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
    <link rel='stylesheet' id='genericons-css'  href='/assets/theme1/wp-content/themes/wddswp/genericons/genericons.css%3Fver=3.2.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wddswp-style-css'  href='/assets/theme1/wp-content/themes/wddswp/style.css%3Fver=4.9.8.css' type='text/css' media='all' />
    <!--[if lt IE 9]>
    <link rel='stylesheet' id='wddswp-ie-css'  href='/assets/theme1/wp-content/themes/wddswp/css/ie.css?ver=20141010' type='text/css' media='all' />
    <![endif]-->
    <!--[if lt IE 8]>
    <link rel='stylesheet' id='wddswp-ie7-css'  href='/assets/theme1/wp-content/themes/wddswp/css/ie7.css?ver=20141010' type='text/css' media='all' />
    <![endif]-->
    <link rel='stylesheet' id='rw_toprated-css'  href='https://secure.rating-widget.com/css/wordpress/toprated.css?ck=Y2019M04D29&#038;ver=3.0.3' type='text/css' media='all' />
    <link rel='stylesheet' id='rw_recommendations-css'  href='https://secure.rating-widget.com/css/widget/recommendations.css?ck=Y2019M04D29&#038;ver=3.0.3' type='text/css' media='all' />
    <link rel='stylesheet' id='heateor_sss_frontend_css-css'  href='/assets/theme1/wp-content/plugins/sassy-social-share/public/css/sassy-social-share-public.css%3Fver=3.2.10.css' type='text/css' media='all' />
    <link rel='stylesheet' id='heateor_sss_sharing_default_svg-css'  href='/assets/theme1/wp-content/plugins/sassy-social-share/admin/css/sassy-social-share-svg.css%3Fver=3.2.10.css' type='text/css' media='all' />
    <link rel='stylesheet' id='simcal-qtip-css'  href='/assets/theme1/wp-content/plugins/google-calendar-events/assets/css/vendor/jquery.qtip.min.css%3Fver=3.1.20.css' type='text/css' media='all' />
    <link rel='stylesheet' id='simcal-default-calendar-grid-css'  href='/assets/theme1/wp-content/plugins/google-calendar-events/assets/css/default-calendar-grid.min.css%3Fver=3.1.20.css' type='text/css' media='all' />
    <link rel='stylesheet' id='simcal-default-calendar-list-css'  href='/assets/theme1/wp-content/plugins/google-calendar-events/assets/css/default-calendar-list.min.css%3Fver=3.1.20.css' type='text/css' media='all' />
    <link rel='stylesheet' id='js_composer_front-css'  href='/assets/theme1/wp-content/plugins/js_composer/assets/css/js_composer.min.css%3Fver=5.4.7.css' type='text/css' media='all' />
    <link rel='stylesheet' id='js_composer_custom_css-css'  href='/assets/theme1/wp-content/uploads/js_composer/custom.css%3Fver=5.4.7.css' type='text/css' media='all' />
    <link rel='stylesheet' id='custom-css-public'  href='/assets/theme1/custom.css' type='text/css' media='all' />
    <script type='text/javascript' src='/assets/theme1/wp-includes/js/jquery/jquery.js%3Fver=1.12.4'></script>
    <script type='text/javascript' src='/assets/theme1/wp-includes/js/jquery/jquery-migrate.min.js%3Fver=1.4.1'></script>
    <link rel='https://api.w.org/' href='wp-json/index.html' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="/assets/theme1/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 4.9.8" />
    <link rel="canonical" href="index.html" />
    <link rel='shortlink' href='index.html' />
    <link rel="alternate" type="application/json+oembed" href="/assets/theme1/wp-json/oembed/1.0/embed%3Furl=https%253A%252F%252Fwww.wdds.ca%252F" />
    <link rel="alternate" type="text/xml+oembed" href="/assets/theme1/wp-json/oembed/1.0/embed%3Furl=https%253A%252F%252Fwww.wdds.ca%252F&amp;format=xml" />
    <script type="text/javascript">
        (function(url){
            if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
            var addEvent = function(evt, handler) {
                if (window.addEventListener) {
                    document.addEventListener(evt, handler, false);
                } else if (window.attachEvent) {
                    document.attachEvent('on' + evt, handler);
                }
            };
            var removeEvent = function(evt, handler) {
                if (window.removeEventListener) {
                    document.removeEventListener(evt, handler, false);
                } else if (window.detachEvent) {
                    document.detachEvent('on' + evt, handler);
                }
            };
            var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
            var logHuman = function() {
                if (window.wfLogHumanRan) { return; }
                window.wfLogHumanRan = true;
                var wfscr = document.createElement('script');
                wfscr.type = 'text/javascript';
                wfscr.async = true;
                wfscr.src = url + '&r=' + Math.random();
                (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
                for (var i = 0; i < evts.length; i++) {
                    removeEvent(evts[i], logHuman);
                }
            };
            for (var i = 0; i < evts.length; i++) {
                addEvent(evts[i], logHuman);
            }
        })('//www.wdds.ca/?wordfence_lh=1&hid=D5BCA315FD5EFA145A21F5E8E71CE2B7');
    </script>
    <meta name="tec-api-version" content="v1">
    <meta name="tec-api-origin" content="/assets/theme1">
    <link rel="https://theeventscalendar.com/" href="/assets/theme1/wp-json/tribe/events/v1/index.html" />
    <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="/assets/theme1/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen">
    <![endif]-->
    <style type="text/css" id="wp-custom-css"></style>
    <style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1531915975156{background-image: url(https://dev.spotmarketing.ca/wdds/wp-content/uploads/2018/07/leaves.png?id=44) !important;}.vc_custom_1531757959116{background-image: url(https://dev.spotmarketing.ca/wdds/wp-content/uploads/2018/07/hire-area-left-img.jpg?id=69) !important;}</style>



    <noscript>
        <style type="text/css">
            .wpb_animate_when_almost_visible { opacity: 1; }
            label{
                display: inline-block !important;
            }
        </style>
    </noscript>

    <script type="text/javascript">
        $(document).ready(function(){
            //alert($("#tab-count").val());
            var count = $("#tab-count").val();

            var current_tab = 0;

            for(i = 1; i < count; i++){
                $(".tab-" + i).hide();
            }

            $("#prev-form-tab").hide();

            $("#next-form-tab").on('click',function(){
                $(".tab-" + current_tab).hide();
                current_tab++;
                $(".tab-" + current_tab).show();

                toggleNextBack(current_tab);

            });

            $("#prev-form-tab").on('click',function(){
                $(".tab-" + current_tab).hide();
                current_tab--;
                $(".tab-" + current_tab).show();

                toggleNextBack(current_tab);

            });

            function toggleNextBack(current_tab){
                if(current_tab > 0){
                    $("#prev-form-tab").show();
                }else{
                    $("#prev-form-tab").hide();
                }

                //alert(current_tab + '  ' + (count-1));

                if(current_tab > (count-2)){
                    $("#next-form-tab").hide();
                    $("#submit-form").show();
                }else{
                    $("#next-form-tab").show();
                    $("#submit-form").hide();
                }
            }


        });

    </script>

</head>
<body class="home page-template-default page page-id-28 page-parent tribe-no-js wpb-js-composer js-comp-ver-5.4.7 vc_responsive" id="maincontent">


{{--
<form>
    <div class="form-group row">
        <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-10">
            <input type="date" readonly class="form-control" id="staticEmail" value="email@example.com">
        </div>
    </div>
    <div class="form-group row">
        <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
        <div class="col-sm-4">
            <input type="password" class="form-control" id="inputPassword" placeholder="Password">
        </div>

        <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
        <div class="col-sm-4">
            <input type="date" class="form-control" id="inputPassword" placeholder="">
        </div>
    </div>
</form>

<input type="date">
--}}

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <brand>
            <div class="entry-content">


                <div class="container">
                    {!! html_entity_decode($form) !!}
                </div>






            </div>
            <!-- .entry-content -->
        </brand>
        <!-- #post-## -->
    </main>
    <!-- .site-main -->
</div>



<!-- This site's ratings are powered by RatingWidget plugin v3.0.3 (Free version) - https://rating-widget.com/wordpress-plugin/ -->
<div class="rw-js-container">
    <script type="text/javascript">
        var defaultRateCallbacks = {};

        // Initialize ratings.
        function RW_Async_Init() {
            RW.init({uid: "3e4579caffb4d35756f44320ae760662", huid: "409767",
                source: "wordpress",
                options: {
                },
                identifyBy: "laccount"
            });
            var options = {"type":"star","size":"medium","theme":"star_flat_yellow"};

            RW.initClass("page", options);
            RW.initRating("290", {title: "Home", url: "https:\/\/www.wdds.ca\/"});							RW.render(function () {
                (function ($) {
                    $('.rw-rating-table:not(.rw-no-labels):not(.rw-comment-admin-rating)').each(function () {
                        var ratingTable = $(this);

                        // Find the current width before floating left or right to
                        // keep the ratings aligned
                        var col1 = ratingTable.find('td:first');
                        var widthCol1 = col1.width();
                        ratingTable.find('td:first-child').width(widthCol1);

                        if (ratingTable.hasClass('rw-rtl')) {
                            ratingTable.find('td').css({float: 'right'});
                        } else {
                            ratingTable.find('td').css({float: 'left'});
                        }
                    });
                })(jQuery);
            }, true);
        }

        RW_Advanced_Options = {
            blockFlash: !(false)
        };

        // Append RW JS lib.
        if (typeof(RW) == "undefined") {
            (function () {
                var rw = document.createElement("script");
                rw.type = "text/javascript";
                rw.async = true;
                rw.src = "https://secure.rating-widget.com/js/external.min.js?ck=Y2019M04D29?wp=3.0.3";
                var s = document.getElementsByTagName("script")[0];
                s.parentNode.insertBefore(rw, s);
            })();
        }
    </script>
</div>
<!-- / RatingWidget plugin -->
<script>
    ( function ( body ) {
        'use strict';
        body.className = body.className.replace( /\btribe-no-js\b/, 'tribe-js' );
    } )( document.body );
</script>
<script> /* <![CDATA[ */var tribe_l10n_datatables = {"aria":{"sort_ascending":": activate to sort column ascending","sort_descending":": activate to sort column descending"},"length_menu":"Show _MENU_ entries","empty_table":"No data available in table","info":"Showing _START_ to _END_ of _TOTAL_ entries","info_empty":"Showing 0 to 0 of 0 entries","info_filtered":"(filtered from _MAX_ total entries)","zero_records":"No matching records found","search":"Search:","all_selected_text":"All items on this page were selected. ","select_all_link":"Select all pages","clear_selection":"Clear Selection.","pagination":{"all":"All","next":"Next","previous":"Previous"},"select":{"rows":{"0":"","_":": Selected %d rows","1":": Selected 1 row"}},"datepicker":{"dayNames":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"dayNamesShort":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],"dayNamesMin":["S","M","T","W","T","F","S"],"monthNames":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesShort":["January","February","March","April","May","June","July","August","September","October","November","December"],"monthNamesMin":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"nextText":"Next","prevText":"Prev","currentText":"Today","closeText":"Done","today":"Today","clear":"Clear"}};var tribe_system_info = {"sysinfo_optin_nonce":"78c9300c9a","clipboard_btn_text":"Copy to clipboard","clipboard_copied_text":"System info copied","clipboard_fail_text":"Press \"Cmd + C\" to copy"};/* ]]> */ </script>
<script type='text/javascript'>
    //<![CDATA[
    (function( $ ) { 'use strict';
        var insert_a11y_toolbar = '<!-- a11y toolbar -->';
        insert_a11y_toolbar += '<div class="a11y-non-responsive a11y-toolbar ltr left">';
        insert_a11y_toolbar += '<ul class="a11y-toolbar-list">';insert_a11y_toolbar += '<li class="a11y-toolbar-list-item"><button type="button" class="a11y-toggle-contrast toggle-contrast" id="is_normal_contrast" aria-pressed="false"><span class="offscreen">Toggle High Contrast</span><span class="aticon aticon-adjust" aria-hidden="true"></span></button></li>';insert_a11y_toolbar += '<li class="a11y-toolbar-list-item"><button type="button" class="a11y-toggle-fontsize toggle-fontsize" id="is_normal_fontsize" aria-pressed="false"><span class="offscreen">Toggle Font size</span><span class="aticon aticon-font" aria-hidden="true"></span></button></li>';
        insert_a11y_toolbar += '</ul>';
        insert_a11y_toolbar += '</div>';
        insert_a11y_toolbar += '<!-- // a11y toolbar -->';
        $( document ).find( 'body' ).prepend( insert_a11y_toolbar );
    }(jQuery));
    //]]>
</script>
<link rel='stylesheet' id='wppcp_front_css-css'  href='/assets/theme1/wp-content/plugins/wp-private-content-plus/css/wppcp-front.css%3Fver=4.9.8.css' type='text/css' media='all' />
<link rel='stylesheet' id='vc_google_fonts_adaminaregular-css'  href='https://fonts.googleapis.com/css?family=Adamina%3Aregular&amp;ver=4.9.8' type='text/css' media='all' />
<script type='text/javascript' src='/assets/theme1/wp-content/plugins/photo-gallery/js/jquery.sumoselect.min.js%3Fver=3.0.3'></script>
<script type='text/javascript' src='/assets/theme1/wp-content/plugins/photo-gallery/js/jquery.mobile.min.js%3Fver=1.5.13'></script>
<script type='text/javascript' src='/assets/theme1/wp-content/plugins/photo-gallery/js/jquery.mCustomScrollbar.concat.min.js%3Fver=1.5.13'></script>
<script type='text/javascript' src='/assets/theme1/wp-content/plugins/photo-gallery/js/jquery.fullscreen-0.4.1.min.js%3Fver=0.4.1'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var bwg_objectL10n = {"bwg_field_required":"field is required.","bwg_mail_validation":"This is not a valid email address.","bwg_search_result":"There are no images matching your search.","is_pro":""};
    /* ]]> */
</script>
<script type='text/javascript' src='/assets/theme1/wp-content/plugins/photo-gallery/js/bwg_gallery_box.js%3Fver=1.5.13'></script>
<script type='text/javascript' src='/assets/theme1/wp-content/plugins/photo-gallery/js/bwg_embed.js%3Fver=1.5.13'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var bwg_objectsL10n = {"bwg_select_tag":"Select Tag","bwg_order_by":"Order By","bwg_search":"Search","bwg_show_ecommerce":"Show Ecommerce","bwg_hide_ecommerce":"Hide Ecommerce","bwg_show_comments":"Show Comments","bwg_hide_comments":"Hide Comments","bwg_restore":"Restore","bwg_maximize":"Maximize","bwg_fullscreen":"Fullscreen","bwg_search_tag":"SEARCH...","bwg_tag_no_match":"No tags found","bwg_all_tags_selected":"All tags selected","bwg_tags_selected":"tags selected","play":"Play","pause":"Pause","is_pro":""};
    /* ]]> */
</script>
<script type='text/javascript' src='/assets/theme1/wp-content/plugins/photo-gallery/js/bwg_frontend.js%3Fver=1.5.13'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var a11y_stylesheet_path = "https:\/\/www.wdds.ca\/wp-content\/plugins\/wp-accessibility\/toolbar\/css\/a11y-contrast.css";
    /* ]]> */
</script>
<script type='text/javascript' src='/assets/theme1/wp-content/plugins/wp-accessibility/toolbar/js/a11y.js%3Fver=1.0'></script>
<script type='text/javascript' src='/assets/theme1/wp-content/plugins/wp-accessibility/js/longdesc.button.js%3Fver=1.0'></script>
<script type='text/javascript' src='/assets/theme1/wp-content/themes/wddswp/js/skip-link-focus-fix.js%3Fver=20141010'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var screenReaderText = {"expand":"<span class=\"screen-reader-text\">expand child menu<\/span>","collapse":"<span class=\"screen-reader-text\">collapse child menu<\/span>"};
    /* ]]> */
</script>
<script type='text/javascript' src='/assets/theme1/wp-content/themes/wddswp/js/functions.js%3Fver=20150330'></script>
<script type='text/javascript' src='/assets/theme1/wp-content/plugins/sassy-social-share/public/js/sassy-social-share-public.js%3Fver=3.2.10'></script>
<script type='text/javascript' src='/assets/theme1/wp-content/plugins/google-calendar-events/assets/js/vendor/jquery.qtip.min.js%3Fver=3.1.20'></script>
<script type='text/javascript' src='/assets/theme1/wp-content/plugins/google-calendar-events/assets/js/vendor/moment.min.js%3Fver=3.1.20'></script>
<script type='text/javascript' src='/assets/theme1/wp-content/plugins/google-calendar-events/assets/js/vendor/moment-timezone-with-data.min.js%3Fver=3.1.20'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var simcal_default_calendar = {"ajax_url":"\/wp-admin\/admin-ajax.php","nonce":"031ef0433c","locale":"en_US","text_dir":"ltr","months":{"full":["January","February","March","April","May","June","July","August","September","October","November","December"],"short":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]},"days":{"full":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"short":["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]},"meridiem":{"AM":"AM","am":"am","PM":"PM","pm":"pm"}};
    /* ]]> */
</script>
<script type='text/javascript' src='/assets/theme1/wp-content/plugins/google-calendar-events/assets/js/default-calendar.min.js%3Fver=3.1.20'></script>
<script type='text/javascript' src='/assets/theme1/wp-content/plugins/google-calendar-events/assets/js/vendor/imagesloaded.pkgd.min.js%3Fver=3.1.20'></script>
<script type='text/javascript' src='/assets/theme1/wp-includes/js/wp-embed.min.js%3Fver=4.9.8'></script>
<script type='text/javascript' src='/assets/theme1/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js%3Fver=5.4.7'></script>
<script type='text/javascript'>
    //<![CDATA[
    (function( $ ) { 'use strict';

        $('a').removeAttr('target');

        $('input,a,select,textarea,button').removeAttr('tabindex');

    }(jQuery));
    //]]>
</script>
<script src="/assets/theme1/wp-content/themes/wddswp/js/bootstrap.min.js"></script>
<script src="/assets/theme1/wp-content/themes/wddswp/js/custom.js"></script>
</body>
</html>