-- MySQL Workbench Synchronization
-- Generated: 2019-05-20 14:10
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: dgl

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `wdds`.`page_contents`
DROP FOREIGN KEY `fk_page_contents_galleries1`;

ALTER TABLE `wdds`.`gallery_photos`
DROP FOREIGN KEY `fk_gallery_photos_files1`,
DROP FOREIGN KEY `fk_gallery_photos_galleries1`;

ALTER TABLE `wdds`.`page_contents`
DROP COLUMN `gallery_id`,
ADD COLUMN `gallery_id` INT(11) NULL DEFAULT NULL AFTER `form_id`,
ADD INDEX `fk_page_contents_galleries1_idx` (`gallery_id` ASC),
DROP INDEX `fk_page_contents_galleries1_idx` ;

ALTER TABLE `wdds`.`form_submissions`
CHANGE COLUMN `content` `content` LONGBLOB NULL DEFAULT NULL ;

ALTER TABLE `wdds`.`gallery_photos`
DROP COLUMN `gallery_id`,
DROP COLUMN `file_id`,
ADD COLUMN `file_id` BIGINT(20) NOT NULL AFTER `id`,
ADD COLUMN `gallery_id` INT(11) NOT NULL AFTER `file_id`,
ADD INDEX `fk_gallery_photos_files1_idx` (`file_id` ASC),
ADD INDEX `fk_gallery_photos_galleries1_idx` (`gallery_id` ASC),
DROP INDEX `fk_gallery_photos_galleries1_idx` ,
DROP INDEX `fk_gallery_photos_files1_idx` ;

ALTER TABLE `wdds`.`page_contents`
ADD CONSTRAINT `fk_page_contents_galleries1`
  FOREIGN KEY (`gallery_id`)
  REFERENCES `wdds`.`galleries` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `wdds`.`gallery_photos`
ADD CONSTRAINT `fk_gallery_photos_files1`
  FOREIGN KEY (`file_id`)
  REFERENCES `wdds`.`files` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_gallery_photos_galleries1`
  FOREIGN KEY (`gallery_id`)
  REFERENCES `wdds`.`galleries` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
