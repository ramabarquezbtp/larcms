-- MySQL Workbench Synchronization
-- Generated: 2019-05-15 10:41
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: dgl

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `wdds_laravel_cms`.`group_routes`
DROP FOREIGN KEY `fk_group_routes_groups1`;

ALTER TABLE `wdds_laravel_cms`.`menu_pages`
DROP FOREIGN KEY `fk_menu_pages_menus1`;

ALTER TABLE `wdds_laravel_cms`.`contents`
DROP FOREIGN KEY `fk_contents_content_types1`,
DROP FOREIGN KEY `fk_contents_users1`;

ALTER TABLE `wdds_laravel_cms`.`page_contents`
ADD COLUMN `gallery_id` INT(11) NULL DEFAULT NULL AFTER `form_id`,
ADD INDEX `fk_page_contents_galleries1_idx` (`gallery_id` ASC);

CREATE TABLE IF NOT EXISTS `wdds_laravel_cms`.`galleries` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS `wdds_laravel_cms`.`gallery_photos` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `file_id` BIGINT(20) NOT NULL,
  `gallery_id` INT(11) NOT NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  INDEX `fk_gallery_photos_files1_idx` (`file_id` ASC),
  INDEX `fk_gallery_photos_galleries1_idx` (`gallery_id` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_gallery_photos_files1`
    FOREIGN KEY (`file_id`)
    REFERENCES `wdds_laravel_cms`.`files` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gallery_photos_galleries1`
    FOREIGN KEY (`gallery_id`)
    REFERENCES `wdds_laravel_cms`.`galleries` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

ALTER TABLE `wdds_laravel_cms`.`group_routes`
ADD CONSTRAINT `fk_group_routes_groups1`
  FOREIGN KEY (`group_id`)
  REFERENCES `wdds_laravel_cms`.`groups` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `wdds_laravel_cms`.`pages`
DROP FOREIGN KEY `fk_pages_users2`;

ALTER TABLE `wdds_laravel_cms`.`pages` ADD CONSTRAINT `fk_pages_users2`
  FOREIGN KEY (`user_id`)
  REFERENCES `wdds_laravel_cms`.`users` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `wdds_laravel_cms`.`user_groups`
DROP FOREIGN KEY `fk_user_groups_users1`;

ALTER TABLE `wdds_laravel_cms`.`user_groups` ADD CONSTRAINT `fk_user_groups_users1`
  FOREIGN KEY (`user_id`)
  REFERENCES `wdds_laravel_cms`.`users` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `wdds_laravel_cms`.`menu_pages`
ADD CONSTRAINT `fk_menu_pages_menus1`
  FOREIGN KEY (`menu_id`)
  REFERENCES `wdds_laravel_cms`.`menus` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `wdds_laravel_cms`.`contents`
ADD CONSTRAINT `fk_contents_content_types1`
  FOREIGN KEY (`content_type_id`)
  REFERENCES `wdds_laravel_cms`.`content_types` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_contents_users1`
  FOREIGN KEY (`user_id`)
  REFERENCES `wdds_laravel_cms`.`users` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `wdds_laravel_cms`.`page_contents`
DROP FOREIGN KEY `fk_page_contents_pages1`;

ALTER TABLE `wdds_laravel_cms`.`page_contents` ADD CONSTRAINT `fk_page_contents_pages1`
  FOREIGN KEY (`page_id`)
  REFERENCES `wdds_laravel_cms`.`pages` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_page_contents_galleries1`
  FOREIGN KEY (`gallery_id`)
  REFERENCES `wdds_laravel_cms`.`galleries` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
