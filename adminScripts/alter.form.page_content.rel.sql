-- MySQL Workbench Synchronization
-- Generated: 2019-05-07 09:10
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: dgl

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `wdds`.`group_routes`
DROP FOREIGN KEY `fk_group_routes_groups1`;

ALTER TABLE `wdds`.`menu_pages`
DROP FOREIGN KEY `fk_menu_pages_menus1`;

ALTER TABLE `wdds`.`contents`
DROP FOREIGN KEY `fk_contents_content_types1`,
DROP FOREIGN KEY `fk_contents_users1`;

ALTER TABLE `wdds`.`page_contents`
DROP FOREIGN KEY `fk_page_contents_contents1`;

ALTER TABLE `wdds`.`page_contents`
CHANGE COLUMN `content_id` `content_id` INT(11) NULL DEFAULT NULL ,
ADD COLUMN `form_id` INT(11) NULL DEFAULT NULL AFTER `content_id`,
ADD INDEX `fk_page_contents_forms1_idx` (`form_id` ASC);

ALTER TABLE `wdds`.`group_routes`
ADD CONSTRAINT `fk_group_routes_groups1`
  FOREIGN KEY (`group_id`)
  REFERENCES `wdds`.`groups` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `wdds`.`pages`
DROP FOREIGN KEY `fk_pages_users2`;

ALTER TABLE `wdds`.`pages` ADD CONSTRAINT `fk_pages_users2`
  FOREIGN KEY (`user_id`)
  REFERENCES `wdds`.`users` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `wdds`.`user_groups`
DROP FOREIGN KEY `fk_user_groups_users1`;

ALTER TABLE `wdds`.`user_groups` ADD CONSTRAINT `fk_user_groups_users1`
  FOREIGN KEY (`user_id`)
  REFERENCES `wdds`.`users` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `wdds`.`menu_pages`
ADD CONSTRAINT `fk_menu_pages_menus1`
  FOREIGN KEY (`menu_id`)
  REFERENCES `wdds`.`menus` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `wdds`.`contents`
ADD CONSTRAINT `fk_contents_content_types1`
  FOREIGN KEY (`content_type_id`)
  REFERENCES `wdds`.`content_types` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_contents_users1`
  FOREIGN KEY (`user_id`)
  REFERENCES `wdds`.`users` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE;

ALTER TABLE `wdds`.`page_contents`
DROP FOREIGN KEY `fk_page_contents_pages1`;

ALTER TABLE `wdds`.`page_contents` ADD CONSTRAINT `fk_page_contents_pages1`
  FOREIGN KEY (`page_id`)
  REFERENCES `wdds`.`pages` (`id`)
  ON DELETE RESTRICT
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_page_contents_contents1`
  FOREIGN KEY (`content_id`)
  REFERENCES `wdds`.`contents` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_page_contents_forms1`
  FOREIGN KEY (`form_id`)
  REFERENCES `wdds`.`forms` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
