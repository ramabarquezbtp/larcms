-- MySQL Workbench Synchronization
-- Generated: 2019-05-21 13:15
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: dgl

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `wdds_laravel_cms`.`gallery_photos`
DROP FOREIGN KEY `fk_gallery_photos_files1`,
DROP FOREIGN KEY `fk_gallery_photos_galleries1`;

ALTER TABLE `wdds_laravel_cms`.`users`
ADD COLUMN `user_states_id` INT(11) NULL DEFAULT NULL AFTER `password`,
ADD INDEX `fk_users_user_states1_idx` (`user_states_id` ASC);

ALTER TABLE `wdds_laravel_cms`.`gallery_photos`
DROP COLUMN `gallery_id`,
DROP COLUMN `file_id`,
ADD COLUMN `file_id` BIGINT(20) NOT NULL AFTER `id`,
ADD COLUMN `gallery_id` INT(11) NOT NULL AFTER `file_id`,
DROP INDEX `fk_gallery_photos_files1_idx` ,
ADD INDEX `fk_gallery_photos_files1_idx` (`file_id` ASC),
DROP INDEX `fk_gallery_photos_galleries1_idx` ,
ADD INDEX `fk_gallery_photos_galleries1_idx` (`gallery_id` ASC);

CREATE TABLE IF NOT EXISTS `wdds_laravel_cms`.`user_states` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

ALTER TABLE `wdds_laravel_cms`.`users`
ADD CONSTRAINT `fk_users_user_states1`
  FOREIGN KEY (`user_states_id`)
  REFERENCES `wdds_laravel_cms`.`user_states` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `wdds_laravel_cms`.`gallery_photos`
ADD CONSTRAINT `fk_gallery_photos_files1`
  FOREIGN KEY (`file_id`)
  REFERENCES `wdds_laravel_cms`.`files` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_gallery_photos_galleries1`
  FOREIGN KEY (`gallery_id`)
  REFERENCES `wdds_laravel_cms`.`galleries` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
