<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    //

    protected $appends = ['content_type','user'];

    public function content_type(){
        return $this->belongsTo('\App\Model\ContentType');
    }

    public function getContentTypeAttribute(){
        return $this->content_type()->first();
    }

    public function user(){
        return $this->belongsTo('\App\Model\User');
    }

    public function getUserAttribute(){
        return $this->user()->first();
    }

    public function pages(){
        return \App\Model\PageContent::select('pages.*')
            ->join('pages', 'pages.id','=','page_contents.page_id')
            ->where('page_contents.content_id',$this->id)
            ->get();
    }
}
