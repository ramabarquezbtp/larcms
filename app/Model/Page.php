<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    //

    protected $appends = ['user','page_state','page_content'];

    public function user(){
        return $this->belongsTo('\App\Model\User');
    }

    public function page_content(){
        return $this->hasMany('\App\Model\PageContent');
    }

    public function page_state(){
        return $this->belongsTo('\App\Model\PageState');
    }

    public function getPageStateAttribute(){
        return $this->page_state()->first();
    }

    public function getUserAttribute(){
        return $this->user()->first();
    }

    public function getPageContentAttribute(){
        return $this->page_content()->get();
    }
}
