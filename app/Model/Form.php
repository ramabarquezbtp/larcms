<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{

    protected $appends = ['form_submission','submission_count'];

    //
    public function form_submission(){
        return $this->hasMany('\App\Model\FormSubmission');
    }

    public function getFormSubmissionAttribute(){
        return $this->form_submission()->get();
    }

    public function getSubmissionCountAttribute(){
        return $this->form_submission()->count();
    }
}
