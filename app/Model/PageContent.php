<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PageContent extends Model
{
    //
    protected  $appends = ['content','form','gallery'];

    public function content(){
        return $this->belongsTo('\App\Model\Content');
    }

    public function form(){
        return $this->belongsTo('\App\Model\Form');
    }

    public function gallery(){
        return $this->belongsTo('\App\Model\Gallery');
    }

    public function getContentAttribute(){
        return $this->content()->first();
    }

    public function getFormAttribute(){
        return $this->form()->first();
    }

    public function getGalleryAttribute(){
        return $this->gallery()->first();
    }

}
