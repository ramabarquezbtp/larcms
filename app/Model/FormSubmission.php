<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FormSubmission extends Model
{

    public function form(){
        return $this->belongsTo('\App\Model\Form')->first();
    }


}
