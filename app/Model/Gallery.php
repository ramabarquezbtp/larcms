<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    //
    protected $appends = ['gallery_photo'];

    public function gallery_photo(){
        return $this->hasMany('\App\Model\GalleryPhoto');
    }

    public function getGalleryPhotoAttribute(){
        return $this->gallery_photo()->get();
    }
}
