<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Support\Facades\Storage;
use voku\helper\HtmlDomParser;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class PublicController extends Controller
{
    //

    public function image($file_id){

        $file = \App\Model\File::find($file_id);

        $this->getHeader($file, storage_path('app/' . $file->path));
    }

    public function thumbnail($file_id){

        $file = \App\Model\File::find($file_id);

        $this->getHeader($file, storage_path('app/' . $file->thumbnail_path));
    }

    public function doc($file_id, $download = 0){
        $file = \App\Model\File::find($file_id);



        if(isset($file->id) && !in_array($file->mime_type, config('app.image_types'))){

            $real_path = storage_path('app/' . $file->path);

            if(is_file($real_path)){

                if($download){
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename="'.$file->file_name.'"');
                    header('Content-Transfer-Encoding: binary');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                    header('Pragma: public');
                    header('Content-Length: ' . $file->size); //Absolute URL
                    ob_clean();
                    flush();
                    echo file_get_contents($real_path);
                }else{
                    header('Content-Type: ' . $file->mime_type);
                    readfile($real_path);
                }

                die();
            }
        }
        return '<h2>File not found</h2>';
    }

    public function news_events(){

        $featured = (array)json_decode(json_encode(\App\Model\Event::where('featured','=',1)
            ->where('published','=',1)
            ->first()));

        //p($featured,1);

        $events = [];

        foreach ((array)json_decode(json_encode(\App\Model\Event::where('featured','!=',1)
            ->where('published','=',1)
            ->get())) as $row){

            $events[] = (array)$row;
        }

        //p($events,1);

        $view_data = [
            'events' => (object)$events,
            'featured' => $featured
        ];

        return view('public.events', $view_data);
    }

    public function news_events_view($uri){

        $obj = \App\Model\Event::where('uri','=',"/{$uri}")
            ->where('published','=',1)
            ->first();

        if(!isset($obj->id)){
            return redirect('/page/not-found');
        }

        $view_data = [
            'rec' => $obj
        ];

        return view('public.eventView', $view_data);
    }

    public function page($page_id = 'Home'){

        $page = \App\Model\Page::where('uri','/'.$page_id)
            ->where('private','=','0')
            ->where('page_state_id','=',1)
            ->first();

        if(!isset($page->id)){
            $page = \App\Model\Page::where('uri','/page-not-found')->first();
        }

        $reCaptcha = '';

        $keyword = Request::get('s');

        $search_content = '';

        if($keyword){
            $search_content = $this->find_keyword();
        }


        $view = 'public.not-found';
        $data = [];

        if(isset($page->id)){

            $view = 'public.page';

            $page = \App\Model\Page::find($page->id);

            if(isset($page->page_content) && $page->page_content){

                foreach ($page->page_content as $selected_content){

                    if($selected_content->form_id){

                        $formObj = new \App\Lib\Form($selected_content->form->content,$selected_content->form->id);

                        $content = [
                            'content' => $formObj->generate(),
                            'edit_link' => url("admin/forms/{$selected_content->form_id}/edit")
                        ];

                        $data['Inner'][$selected_content->order][] = (object)$content;

                        if($reCaptcha === ''){
                            $reCaptcha = '<script src="https://www.google.com/recaptcha/api.js?render='.env('SITE_KEY').'"></script>';
                        }

                    }elseif($selected_content->gallery_id){

                        $rec = \App\Model\Gallery::find($selected_content->gallery_id);

                        //p(json_encode($rec,128),1);

                        $gallery = ['bullets'=>'','items'=>''];

                        $active[0] = 'active';

                        $style = 'style="width:25%; margin: 0; padding: 0"';
                        $imageStyle = 'style="width:100%;"';


                        foreach ($rec->gallery_photo as $index => $row){
                            //$gallery['bullets'] .= '<li data-target="#myCarousel" data-slide-to="'.$index.'" class="'.((isset($active[$index]))?'active':'').'"></li>';
                            $gallery['items'] .= '<li class="col-xs-6 col-sm-4 col-md-3" data-responsive="" data-src="'.url('files/image/' . $row->file_id).'" data-sub-html="" '.$style.'>
                                    <a href="" '.$imageStyle.'>
                                        <img class="img-responsive" src="'.url('files/thumbnail/' . $row->file_id).'" '.$imageStyle.'>
                                    </a>
                                </li>';
                        }

                        $content = [
                            'content' => view("admin.gallery.images",['gallery' => $gallery]),
                            'edit_link' => url("admin/galleries/{$selected_content->gallery_id}/edit")
                        ];

                        $data['Inner'][$selected_content->order][] = (object)$content;
                    }else{

                        $content_type = $selected_content->content->content_type->name;

                        $content = $selected_content->content;

                        if($content_type != 'Inner'){
                            $data[$content_type] = $content;
                        }else{
                            if($search_content){

                                $content->content = $search_content;
                            }

                            $content->edit_link = url("admin/contents/{$content->id}/edit");

                            $data[$content_type][$selected_content->order][] = $content;

                        }
                    }
                }
                if(isset($data['Inner'])){
                    ksort($data['Inner']);
                }
            }

            $data['page'] = $page;
            $data['reCaptcha'] =  $reCaptcha;
        }

        return view($view,$data);
    }

    public function find_keyword(){

        $keyword = Request::get('s');

        $res = \App\Model\Page::join('page_contents', 'pages.id', '=', 'page_contents.page_id')
            ->join('contents','page_contents.content_id','=','contents.id')
            ->join('content_types','contents.content_type_id','=','content_types.id')
            ->select('pages.*')
            //->select('contents.*')
            ->whereRaw("(contents.content LIKE '%{$keyword}%' OR contents.title LIKE '%{$keyword}%' OR pages.title LIKE '%{$keyword}%') AND content_types.name = 'Inner'")
            ->get();

        $result = [];

        foreach ($res as $index => $row){
            $inner_content = '';
            $result[$index]['title'] = $row->title;
            $result[$index]['uri'] = $row->uri;
            foreach ($row->page_content as $row1){

                if(isset($row1->content->content_type->name) && $row1->content->content_type->name == 'Inner'){
                    $inner_content = $row1->content->content;
                    break;
                }
            }

            if($inner_content){
                $dom = HtmlDomParser::str_get_html($inner_content);

                foreach ($dom->find('a, span, img') as $a){
                    $a->outertext = '';
                }

                $dom->load($dom->save());

                $result[$index]['h1'] = $dom->findOne('h1')->innertext;
                $result[$index]['p'] = '';
                foreach ($dom->find('p') as $p){
                    $result[$index]['p'] .= $p->innertext;
                }
            }
        }

        $view_data = [
            'keyword' => $keyword,
            'result' => $result
        ];

        return view('public.search',$view_data);
    }

    public function testing(){
        //return view('public');


        return \App\Lib\MyMail::send('ram.abarquez.nettrac@gmail.com','Ramonito Aba', 'Calculus','body and soul');
    }

    public function save_form(){
        //save_form
        //p(Request::input());
        $form_submission = new \App\Model\FormSubmission();
        $input = Request::input();

        if(isset($_FILES)){
            foreach ($_FILES as $index => $row){
                $row['tmp_name'] = base64_encode(file_get_contents($row['tmp_name']));
                $input[$index] = $row;

            }
        }

        $form_submission->form_id = Request::input('form_id');
        $form_submission->content = json_encode($input);

        $captcha = Request::input('token');

        $secretKey = env('SECRET_KEY');

        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data = array('secret' => $secretKey, 'response' => $captcha);

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );

        $context  = stream_context_create($options);
        $response = file_get_contents($url, false, $context);
        $responseKeys = json_decode($response,true);

        if($responseKeys["success"]) {
            $form_submission->save();
            return json_encode(array('success' => 'true', 'capt' => $captcha, 'token' => $_POST['token']));
        } else {
            return json_encode(array('success' => 'false',$responseKeys));
        }



        //return redirect('page/thank-you');
    }

    public function download_file(){
        //header('Content-Type: ' . Request::input('mime-type'));

        $arr = explode(";base64,",Request::input('content'));

        $mime_type = str_replace("data:","",$arr[0]);

        $ext = [
            'image/png' => '.png',
            'image/jpeg'=> '.jpg',
            'image/gif' => '.gif',
            'application/pdf' => '.pdf',
            'application/vnd.ms-excel' => '.xls',
            'application/msword' => '.doc',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => '.docx',
            'text/csv' => '.csv',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'xls'
        ];

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="Download'.$ext[$mime_type].'"');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        //header('Content-Length: ' . Request::input('size')); //Absolute URL



        ob_clean();
        flush();
        echo base64_decode($arr[1]);
        exit();

    }

    private function getHeader($file, $path){

        $content_type = 'image/png';
        $real_path = public_path('assets/images/download.png');

        //p(storage_path("app/{$file->path}"),1);

        if(isset($file->id) && is_file(storage_path("app/{$file->path}")) && in_array($file->mime_type, config('app.image_types'))){

            $content_type = $file->mime_type;
            $real_path = $path;

        }elseif(isset($file->id) && is_file(storage_path("app/{$file->path}"))){

            $real_path = public_path('assets/thumbnails/doc.png');
        }

        header('Content-Type: ' . $content_type);
        readfile($real_path);
    }

}
