<?php

namespace App\Http\Controllers\admin;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{

    private $sort_field = 'title';
    private $sort_order = 'asc';
    private $main_table = 'pages';
    private $controller = 'pages';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $keyword = Request::get('keyword');

        $where = "1";

        $session_keyword_key = "{$this->controller}.keyword";


        if(Request::has('keyword') || Request::get('keyword')){
            $keyword = Request::get('keyword');
            Request::session()->put($session_keyword_key, $keyword);
        }elseif(Request::session()->get($session_keyword_key)){
            $keyword = Request::session()->get($session_keyword_key);
        }

        if($keyword){
            $where = "pages.title LIKE '%{$keyword}%' OR pages.uri LIKE '%{$keyword}%'";
        }

        $config = [
            'controller' => $this->main_table,
            'sort_header' => [
                'title' => ['Title', 'asc', ''],
                'uri' => ['URI', 'asc', ''],
                'users.name' => ['Created by', 'asc', ''],
                'page_states.name' => ['Status', 'asc', ''],
                'pages.created_at' => ['Created at', 'asc', ''],
                'pages.updated_at' => ['Updated at', 'asc', '']
            ]
        ];

        $sorter = new \App\Lib\Sorter($config, $this->sort_field, $this->sort_order);

        $res = \App\Model\Page::orderBy($this->sort_field, $this->sort_order)
            ->join('users', "{$this->main_table}.user_id",'=','users.id')
            ->join('page_states', "{$this->main_table}.page_state_id",'=','page_states.id')
            ->select("{$this->main_table}.*")
            ->whereRaw($where)
            ->paginate(env('ADMIN_PAGE_LIMIT'));

        //p(json_encode($res,JSON_PRETTY_PRINT),1);

        $data = [
            'rec' => $res,
            'sort_field' => $this->sort_field,
            'sort_order' => $this->sort_order,
            'header' => $sorter->getHeader(),
            'controller' => $this->controller,
            'keyword' => $keyword
        ];

        return view('admin.page.list',$data);
        //return view('admin.main',['pages' => $pages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $contents = [];

        if(\App\Model\Content::count() > 0){
            foreach (\App\Model\Content::orderBy('title','asc')->get() as $content){
                $contents[$content->content_type->name][] = $content;
            }
        }

        $view_data = [
            'contents' => $contents,
            'forms' => \App\Model\Form::orderBy('name','asc')->get(),
            'galleries' => \App\Model\Gallery::orderBy('name','asc')->get()
        ];

        //p(json_encode($view_data['galleries'],128),1);

        return view('admin.page.create',$view_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $page = new \App\Model\Page();
        $page_state = 2;
        $private = 0;

        if(Request::input('page_state_id')){
            $page_state = 1;
        }

        if(Request::input('private')){
            $private = 1;
        }

        $page->title = Request::input('title');
        $page->uri = Request::input('uri');
        $page->head = Request::input('head');
        $page->display_banner = Request::input('display_banner');
        $page->user_id = Auth::id();
        $page->page_state_id = $page_state;
        $page->private = $private;

        $content_ids = Request::input('content_id');
        $form_ids = Request::input('form_id');
        $gallery_ids = Request::input('gallery_id');
        $content_orders = Request::input('content_order');

        $page->save();

        $content_data = [];

        $i = -1;

        if(is_array($content_ids) && count($content_ids) > 0){
            foreach ($content_ids as $i => $v){
                $content_data[] = ['page_id' => $page->id,'content_id' => $v,'order' => (int)$content_orders[$i]];
            }
            \App\Model\PageContent::insert($content_data);
        }

        if(isset($form_ids[0]) && (int)$form_ids[0] > 0){
            $content_data = [];
            $content_data[] = ['page_id' => $page->id,'form_id' => (int)$form_ids[0],'order' => (int)$content_orders[($i+1)]];
            \App\Model\PageContent::insert($content_data);
            $i++;
        }

        if(isset($gallery_ids[0]) && (int)$gallery_ids[0] > 0){
            $content_data = [];
            $content_data[] = ['page_id' => $page->id,'gallery_id' => (int)$gallery_ids[0],'order' => (int)$content_orders[($i+1)]];
            \App\Model\PageContent::insert($content_data);
        }

        return redirect('admin/' .  $this->main_table)->with('success', config('app.alert_messages.save_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $page = \App\Model\Page::find($id);

        //p(json_encode($page,128),1);

        $selected_contents = [];
        $selected_forms = [];
        $selected_galleries = [];

        //p(json_encode($page->page_content[4],128),1);

        if(isset($page->page_content) && $page->page_content){
            foreach ($page->page_content as $selected_content){
                if(isset($selected_content->content->id)){
                    $selected_contents[] = $selected_content->content->id;
                }elseif(isset($selected_content->form->id)){
                    $selected_forms[] = $selected_content->form->id;
                }elseif (isset($selected_content->gallery->id)){
                    $selected_galleries[] = $selected_content->gallery->id;
                }

            }
        }

        $contents = [];

        if(\App\Model\Content::count() > 0){
            foreach (\App\Model\Content::all() as $content){
                $contents[$content->content_type->name][] = $content;
            }
        }

        $content_data = [
            'page' => $page,
            'contents' => $contents,
            'selected_contents' => $selected_contents,
            'selected_forms' => $selected_forms,
            'selected_galleries' => $selected_galleries,
            'forms' => \App\Model\Form::all(),
            'galleries' => \App\Model\Gallery::all()
        ];

        return view('admin.page.edit',$content_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        p($id);

        //
        return 'you';
        //p(Request::get('title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

    }

    public function save_changes(){
        $id = Request::input('id');
        $message = config('app.alert_messages.record_not_found');
        $page = \App\Model\Page::find($id);

        $page_state = 2;
        if(Request::input('page_state_id')){
            $page_state = 1;
        }

        $private = 0;
        if(Request::input('private')){
            $private = 1;
        }

        if(isset($page->id) && $page->id){
            $page->title = Request::input('title');
            $page->uri = Request::input('uri');
            $page->head = Request::input('head');
            $page->display_banner = Request::input('display_banner');
            $page->page_state_id = $page_state;
            $page->private = $private;

            $page->save();
            $message = config('app.alert_messages.update_success');
        }


        $content_ids = Request::input('content_id');
        $form_ids = Request::input('form_id');
        $gallery_ids = Request::input('gallery_id');
        $content_orders = Request::input('content_order');

        $content_data = [];
        $form_data = [];
        $gallery_data = [];

        //p($content_ids,1);

        if(is_array($content_ids) && count($content_ids) > 0){
            foreach ($content_ids as $i => $v){
                $content_data[] = ['page_id' => $page->id,'content_id' => $v,'order' => (int)$content_orders[$i]];
            }
        }

        if(isset($form_ids[0]) && (int)$form_ids[0] > 0){
            $form_data[] = ['page_id' => $page->id,'form_id' => (int)$form_ids[0],'order' => (int)$content_orders[($i+1)]];
            $i++;
        }

        if(isset($gallery_ids[0]) && (int)$gallery_ids[0] > 0){
            $gallery_data[] = ['page_id' => $page->id,'gallery_id' => (int)$gallery_ids[0],'order' => (int)$content_orders[($i+1)]];
        }

        if(count($content_data) > 0 || count($form_data) > 0){
            \DB::table('page_contents')->where('page_id',$page->id)->delete();
        }

        if(count($content_data) > 0){
            \App\Model\PageContent::insert($content_data);
        }

        if(count($form_data) > 0){
            \App\Model\PageContent::insert($form_data);
        }

        if(count($gallery_data) > 0){
            \App\Model\PageContent::insert($gallery_data);
        }

        return redirect('/admin/' . $this->main_table)->with('success', $message);
    }

    public function remove(){

        $page =  \App\Model\Page::find(Request::input('id'));
        $message = 'Record not deleted';
        if(isset($page->id) && $page->id){
            \DB::table('page_contents')->where('page_id',$page->id)->delete();
            $page->delete();
            $message = config('app.alert_messages.delete_success');
        }

        return redirect('/admin/' . $this->main_table)->with('success', $message);
    }
}
