<?php

namespace App\Http\Controllers\admin;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Request;
use App\Http\Controllers\Controller;

class ContentController extends Controller
{
    private $sort_field = 'title';
    private $sort_order = 'asc';
    private $main_table = 'contents';
    private $controller = 'contents';
    private $view_folder = 'content';
    private $mgt_name = 'Contents';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $keyword = Request::get('keyword');

        $where = "1";

        $session_keyword_key = "{$this->controller}.keyword";


        if(Request::has('keyword') || Request::get('keyword')){
            $keyword = Request::get('keyword');
            Request::session()->put($session_keyword_key, $keyword);
        }elseif(Request::session()->get($session_keyword_key)){
            $keyword = Request::session()->get($session_keyword_key);
        }

        if($keyword){
            $where = "contents.title LIKE '%{$keyword}%' ";
        }

        $config = [
            'controller' => $this->controller,
            'sort_header' => [
                'title' => ['Title', 'asc', ''],
                'content_types.name' => ['Type', 'asc', ''],
                'users.name' => ['Created by', 'asc', ''],
                'contents.created_at' => ['Created at', 'asc', ''],
                'contents.updated_at' => ['Updated at', 'asc', '']
            ]
        ];

        $sorter = new \App\Lib\Sorter($config, $this->sort_field, $this->sort_order);

        $res = \App\Model\Content::orderBy($this->sort_field, $this->sort_order)
            ->join('content_types', 'contents.content_type_id','=','content_types.id')
            ->join('users', 'contents.user_id','=','users.id')
            ->select('contents.*')
            ->whereRaw($where)
            ->paginate(env('ADMIN_PAGE_LIMIT'));

        //p($res[0]->pages(),1);

        $data = [
            'rec' => $res,
            'sort_field' => $this->sort_field,
            'sort_order' => $this->sort_order,
            'header' => $sorter->getHeader(),
            'controller' => $this->controller,
            'mgt_name' => $this->mgt_name,
            'keyword' => $keyword
        ];

        return view('admin.'.$this->view_folder.'.list',$data);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.'.$this->view_folder.'.create',['mgt_name' => $this->mgt_name]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $obj = new \App\Model\Content();

        $obj->title = Request::input('title');
        $obj->content_type_id = Request::input('content_type_id');
        $obj->content = Request::input('content');
        $obj->user_id = Auth::id();

        $obj->save();

        return redirect('admin/' . $this->controller)->with('success', config('app.alert_messages.save_success'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $obj = \App\Model\Content::find($id);

        //p($content->title);

        return view('admin.content.edit',['content' => $obj]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        p($id);

        //
        return 'you';
        //p(Request::get('title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

    }

    public function save_changes(){
        $id = Request::input('id');
        $message = config('app.alert_messages.record_not_found');
        $content = \App\Model\Content::find($id);

        if(isset($content->id) && $content->id){
            $content->title = Request::input('title');
            $content->content = Request::input('content');
            $content->content_type_id = Request::input('content_type_id');
            $content->save();
            $message = config('app.alert_messages.update_success');
        }


        return redirect('/admin/' . $this->controller)->with('success', $message);
    }

    public function find_and_replace(){
        $find = Request::input('find');
        $action = Request::input('action');
        $replacement = Request::input('replacement');
        $message = "";

        $result = \App\Model\Content::where('content', 'LIKE', "%{$find}%")->get();

        if($result && $action == 'find'){

            $message .= '<span> The following content(s) contain\'s the substring "'.$find.'"<span><ul>';
            foreach ($result as $row){
                $message .= '<li>'.$row->title.'</li>';
            }
            $message .= '</ul>';
        }

        if($result && $action == 'replace'){
            $message .= '<span>The following content(s) was updated<span><ul>';
            foreach ($result as $row){
                $message .= '<li>'.$row->title.'</li>';
                \DB::table($this->main_table)
                    ->where('id',$row->id)
                    ->update(['content' => str_replace($find,$replacement,$row->content)]);
            }
            $message .= '</ul>';
        }

        return redirect('/admin/' . $this->controller)->with('success', $message);
    }

    public function remove(){

        $content =  \App\Model\Content::find(Request::input('id'));
        $message = config('app.alert_messages.record_not_found');
        if(isset($content->id) && $content->id){
            $content->delete();
            $message = config('app.alert_messages.delete_success');
        }

        return redirect('/admin/' . $this->controller)->with('success', $message);
    }
}
