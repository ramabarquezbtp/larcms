<?php

namespace App\Http\Controllers\admin;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Request;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{

    private $sort_field = 'name';
    private $sort_order = 'asc';
    private $main_table = 'galleries';
    private $controller = 'galleries';
    private $view_folder = 'gallery';
    private $mgt_name = 'Galleries';
    private $main_model = '\App\Model\Gallery';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $config = [
            'controller' => $this->controller,
            'sort_header' => [
                'name' => ['Name', 'asc', ''],
                $this->main_table.'.created_at' => ['Created at', 'asc', ''],
                $this->main_table.'.updated_at' => ['Updated at', 'asc', '']
            ]
        ];

        $sorter = new \App\Lib\Sorter($config, $this->sort_field, $this->sort_order);

        $res = $this->main_model::orderBy($this->sort_field, $this->sort_order)
            ->paginate(env('ADMIN_PAGE_LIMIT'));

        $data = [
            'rec' => $res,
            'sort_field' => $this->sort_field,
            'sort_order' => $this->sort_order,
            'header' => $sorter->getHeader(),
            'controller' => $this->controller,
            'mgt_name' => $this->mgt_name
        ];

        return view('admin.'.$this->view_folder.'.list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $photos = \App\Model\File::whereIn('mime_type',config('app.pic_types'))
            ->get();

        $view_data = [
            'mgt_name' => $this->mgt_name,
            'view_folder' => $this->view_folder,
            'photos' => $photos,
            'controller' => $this->controller
        ];

        return view('admin.'.$this->view_folder.'.create',$view_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $obj = new $this->main_model();
        //p(Request::input(),1);

        $obj->name = Request::input('name');

        $obj->save();

        $contents = explode(",",Request::input('contents'));

        $content_to_save = [];

        //p($contents,1);

        if(is_array($contents) && count($contents) > 0 && $contents[0]){
            foreach ($contents as $row){
                $content_to_save[] = [
                    'gallery_id' => $obj->id,
                    'file_id' => $row
                ];
            }

            \App\Model\GalleryPhoto::insert($content_to_save);
        }

        return redirect('admin/' . $this->controller)->with('success', config('app.alert_messages.save_success'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $rec = \App\Model\Gallery::find($id);

        //p(json_encode($rec,128),1);

        $gallery = ['bullets'=>'','items'=>''];

        $active[0] = 'active';

        $style = 'style="width:25%; margin: 0; padding: 0"';
        $imageStyle = 'style="width:100%;"';

        foreach ($rec->gallery_photo as $index => $row){
            $gallery['bullets'] .= '<li data-target="#myCarousel" data-slide-to="'.$index.'" class="'.((isset($active[$index]))?'active':'').'"></li>';
            /*$gallery['items'] .= '<li class="col-xs-6 col-sm-4 col-md-3" data-responsive="" data-src="'.url('files/image/' . $row->file_id).'" data-sub-html="">
                        <a href="">
                            <img class="img-responsive" src="'.url('files/thumbnail/' . $row->file_id).'">
                        </a>
                    </li>';*/
            $gallery['items'] .= '
                <li class="col-xs-6 col-sm-4 col-md-3" data-responsive="" data-src="'.url('files/image/' . $row->file_id).'" data-sub-html="" '.$style.'>
                    <a href="" '.$imageStyle.'>
                        <img class="img-responsive" src="'.url('files/thumbnail/' . $row->file_id).'" '.$imageStyle.'>
                    </a>
                </li>';
        }

        $view_data = [
            'mgt_name' => $this->mgt_name
            ,'rec' => $rec
            ,'gallery' => $gallery
        ];
        return view("admin.{$this->view_folder}.view", $view_data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $photos = \App\Model\File::whereIn('mime_type',config('app.pic_types'))
            ->get();

        $rec = $this->main_model::Find($id);

        //p(json_encode($rec,128),1);

        $contents = [];

        $content_images = "";

        if($rec->gallery_photo){
            foreach ($rec->gallery_photo as $row){
                $contents[] = $row->file_id;
                $content_images .= '<div class="col-lg-3 col-md-4 col-6">
                                <a href="#" class="d-block mb-4 h-100 image-selection">
                                    <img class="img-fluid img-thumbnail" src="'.url("files/thumbnail/{$row->file_id}").'" alt="">
                                </a>
                            </div>';
            }
        }

        $view_data = [
            'mgt_name' => $this->mgt_name,
            'view_folder' => $this->view_folder,
            'photos' => $photos,
            'controller' => $this->controller,
            'rec' => $rec,
            'contents' => $contents,
            'content_images' => $content_images
        ];

        return view('admin.'.$this->view_folder.'.edit',$view_data);
    }

    public function save_changes(){

        $id = Request::input('id');

        $obj = $this->main_model::Find($id);

        $obj->name = Request::input('name');

        $obj->save();

        $contents = explode(",",Request::input('contents'));

        $content_to_save = [];

        if(is_array($contents) && count($contents) > 0 && $contents[0]){
            \App\Model\GalleryPhoto::where('gallery_id', $id)->delete();
            foreach ($contents as $row){
                $content_to_save[] = [
                    'gallery_id' => $obj->id,
                    'file_id' => $row
                ];
            }

            \App\Model\GalleryPhoto::insert($content_to_save);
        }

        return redirect('admin/' . $this->controller)->with('success', config('app.alert_messages.update_success'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function remove(){

        $rec =  $this->main_model::find(Request::input('id'));
        $message = config('app.alert_messages.record_not_found');
        if(isset($rec->id) && $rec->id){

            \App\Model\GalleryPhoto::where('gallery_id',$rec->id)->delete();

            $rec->delete();
            $message = config('app.alert_messages.delete_success');
        }

        return redirect('/admin/' . $this->controller)->with('success', $message);
    }

    public function view_gallery($id, $i = 0){

        $rec = \App\Model\Gallery::find($id);

        //p(json_encode($rec,128),1);

        $gallery = ['bullets'=>'','items'=>''];

        $active[$i] = 'active';

        foreach ($rec->gallery_photo as $index => $row){
            $gallery['bullets'] .= '<li data-target="#myCarousel" data-slide-to="'.$index.'" class="'.((isset($active[$index]))?'active':'').'"></li>';
            $gallery['items'] .= '<li class="col-xs-6 col-sm-4 col-md-3" data-responsive="" data-src="'.url('files/image/' . $row->file_id).'" data-sub-html="">
                        <a href="">
                            <img class="img-responsive" src="'.url('files/thumbnail/' . $row->file_id).'">
                        </a>
                    </li>';
        }


        $view_data = [
            'mgt_name' => $this->mgt_name
            ,'rec' => $rec,
            'gallery' => $gallery
        ];

        return view("admin.{$this->view_folder}.viewGallery", $view_data);
    }
}
