<?php

namespace App\Http\Controllers\admin;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    private $sort_field = 'name';
    private $sort_order = 'asc';
    private $main_table = 'users';
    private $controller = 'users';
    private $view_folder = 'user';
    private $mgt_name = 'Users';
    private $main_model = '\App\Model\User';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $keyword = Request::get('keyword');

        $where = "1";

        $session_keyword_key = "{$this->controller}.keyword";


        if(Request::has('keyword') || Request::get('keyword')){
            $keyword = Request::get('keyword');
            Request::session()->put($session_keyword_key, $keyword);
        }elseif(Request::session()->get($session_keyword_key)){
            $keyword = Request::session()->get($session_keyword_key);
        }

        if($keyword){
            $where = "{$this->main_table}.name LIKE '%{$keyword}%' OR {$this->main_table}.email LIKE '%{$keyword}%'";
        }

        $config = [
            'controller' => $this->controller,
            'sort_header' => [
                'name' => ['Name', 'asc', ''],
                'email' => ['Email', 'asc', ''],
                $this->main_table.'.created_at' => ['Created at', 'asc', ''],
                $this->main_table.'.updated_at' => ['Updated at', 'asc', ''],
                'user_states.name' => ['Status', 'asc', '']
            ]
        ];

        $sorter = new \App\Lib\Sorter($config, $this->sort_field, $this->sort_order);

        $res = $this->main_model::orderBy($this->sort_field, $this->sort_order)
            ->join('user_states','users.user_state_id','=','user_states.id')
            ->select('users.*')
            ->whereRaw($where)
            ->paginate(env('ADMIN_PAGE_LIMIT'));

        $data = [
            'rec' => $res,
            'sort_field' => $this->sort_field,
            'sort_order' => $this->sort_order,
            'header' => $sorter->getHeader(),
            'controller' => $this->controller,
            'mgt_name' => $this->mgt_name,
            'keyword' => $keyword
        ];

        //p(json_encode($res,128),1);

        return view('admin.'.$this->view_folder.'.list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view_data = [
            'mgt_name' => $this->mgt_name,
            'view_folder' => $this->view_folder,
            'controller' => $this->controller
        ];

        return view('admin.'.$this->view_folder.'.create',$view_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Request::validate([
            'email' => "required|unique:{$this->main_table}"
        ]);

        //
        $obj = new $this->main_model();
        //p(Request::input(),1);
        $passowd = Request::input('password');

        $obj->name = Request::input('name');
        $obj->email = Request::input('email');
        $obj->password = Hash::make($passowd);
        $obj->user_state_id = 1;

        $obj->save();

        $view_data = [
            'email'=>$obj->email,
            'password'=>$passowd,
            'name' => $obj->name,
            'login_link'=>url("login?u={$obj->email}&p={$passowd}")
        ];

        $body = view("admin.user.mail",$view_data);

        \App\Lib\MyMail::simpleMail($obj->email,'WDDS: Invitation to login CSM', $body);

        return redirect('admin/' . $this->controller)->with('success', config('app.alert_messages.save_success'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $rec = \App\Model\Gallery::find($id);

        //p(json_encode($rec,128),1);

        $gallery = ['bullets'=>'','items'=>''];

        $active[0] = 'active';

        foreach ($rec->gallery_photo as $index => $row){
            $gallery['bullets'] .= '<li data-target="#myCarousel" data-slide-to="'.$index.'" class="'.((isset($active[$index]))?'active':'').'"></li>';
            $gallery['items'] .= '<li class="col-xs-6 col-sm-4 col-md-3" data-responsive="" data-src="'.url('files/image/' . $row->file_id).'" data-sub-html="">
                        <a href="">
                            <img class="img-responsive" src="'.url('files/thumbnail/' . $row->file_id).'">
                        </a>
                    </li>';
        }

        $view_data = [
            'mgt_name' => $this->mgt_name
            ,'rec' => $rec
            ,'gallery' => $gallery
        ];
        return view("admin.{$this->view_folder}.view", $view_data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rec = $this->main_model::Find($id);

        $view_data = [
            'mgt_name' => $this->mgt_name,
            'view_folder' => $this->view_folder,
            'controller' => $this->controller,
            'rec' => $rec,
        ];

        return view('admin.'.$this->view_folder.'.edit',$view_data);
    }

    public function save_changes(){

        $id = Request::input('id');

        Request::validate([
            'email' => "required|unique:{$this->main_table},email,$id,id"
        ]);

        $obj = $this->main_model::Find($id);

        $obj->name = Request::input('name');
        $obj->email = Request::input('email');
        $obj->user_state_id = Request::input('user_state_id');

        if(Request::input('password')){
            $password = Request::input('password');

            $obj->password = Hash::make($password);

            $view_data = [
                'email'=>$obj->email,
                'password'=>$password,
                'name' => $obj->name,
                'login_link'=>url("login?u={$obj->email}&p={$password}")
            ];

            $body = view("admin.user.mail",$view_data);

            \App\Lib\MyMail::simpleMail($obj->email,'WDDS: Your password is updated to login CSM', $body);
            \App\Lib\MyMail::send($obj->email,$obj->name,'WDDS: Your password is updated to login CSM', $body);


        }

        $obj->save();

        return redirect('admin/' . $this->controller)->with('success', config('app.alert_messages.update_success'));
        // http://lar.wdds.ca/login?u=ram.abarquez@nettrac.net&p=#PasswordIsReal .
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function remove(){

        $rec =  $this->main_model::find(Request::input('id'));
        $message = config('app.alert_messages.record_not_found');

        if(isset($rec->id) && $rec->id){

            $rec->user_state_id = \App\Model\UserState::where('name','blocked')->first()->id;

            $rec->save();
            $message = config('app.alert_messages.delete_success');
        }

        return redirect('/admin/' . $this->controller)->with('success', $message);
    }

    public function view_gallery($id, $i = 0){

        $rec = \App\Model\Gallery::find($id);

        $gallery = ['bullets'=>'','items'=>''];

        $active[$i] = 'active';

        foreach ($rec->gallery_photo as $index => $row){
            $gallery['bullets'] .= '<li data-target="#myCarousel" data-slide-to="'.$index.'" class="'.((isset($active[$index]))?'active':'').'"></li>';
            $gallery['items'] .= '<li class="col-xs-6 col-sm-4 col-md-3" data-responsive="" data-src="'.url('files/image/' . $row->file_id).'" data-sub-html="">
                        <a href="">
                            <img class="img-responsive" src="'.url('files/thumbnail/' . $row->file_id).'">
                        </a>
                    </li>';
        }


        $view_data = [
            'mgt_name' => $this->mgt_name
            ,'rec' => $rec,
            'gallery' => $gallery
        ];

        return view("admin.{$this->view_folder}.viewGallery", $view_data);
    }
}
