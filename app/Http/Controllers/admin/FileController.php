<?php

namespace App\Http\Controllers\admin;

use http\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use Intervention\Image\ImageManagerStatic as Image;




class FileController extends Controller
{

    private $sort_field = 'created_at';
    private $sort_order = 'desc';
    private $main_table = 'files';
    private $controller = 'files';


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $keyword = Request::get('keyword');

        $where = "1";

        $session_keyword_key = "{$this->controller}.keyword";


        if(Request::has('keyword') || Request::get('keyword')){
            $keyword = Request::get('keyword');
            Request::session()->put($session_keyword_key, $keyword);
        }elseif(Request::session()->get($session_keyword_key)){
            $keyword = Request::session()->get($session_keyword_key);
        }


        if($keyword){
            $where = "files.file_name LIKE '%{$keyword}%'";
        }

        $files = \App\Model\File::orderBy($this->sort_field,$this->sort_order)
            ->whereRaw($where)
            ->paginate(18);

        //p(config('app.image_types'),1);

        $view_data = [
            'files' => $files,
            'controller' => $this->controller,
            'keyword' => $keyword
        ];

        return view('admin.file.list',$view_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.file.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        if(Request::hasFile('file')){

            $file = Request::file('file');

            $data = new \App\Model\File();

            $data->file_name = $file->getClientOriginalName();
            $data->mime_type = $file->getClientMimeType();
            $data->size = $file->getClientSize();
            $data->error = $file->getError();
            $data->path = $file->store('files');
            $data->user_id = Auth::id();

            if(!in_array($data->mime_type,config('app.allowed_file_types'))){
                throw new InvalidArgumentException('Invalid file type');
            }

            if(in_array($data->mime_type,config('app.image_types'))){
                $image_resize = Image::make($file->getRealPath());

                $thumbnail_path = 'files/' . uniqid() . '.' . $file->getClientOriginalExtension();

                $image_resize->resize(400, null, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $image_resize->save(storage_path('app/' . $thumbnail_path));

                $data->thumbnail_path = $thumbnail_path;
            }

            $data->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function remove(){

        $file =  \App\Model\File::find(Request::input('id'));
        $message = 'Record not deleted';
        if(isset($file->id) && $file->id){
            if($file->thumbnail_path){
                Storage::delete($file->thumbnail_path);
            }

            Storage::delete($file->path);

            $file->delete();
            $message = config('app.alert_messages.delete_success');
        }

        return redirect('/admin/files')->with('success', $message);
    }

}
