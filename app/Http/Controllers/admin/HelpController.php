<?php

namespace App\Http\Controllers\admin;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Request;
use App\Http\Controllers\Controller;

class HelpController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    public function index($file){

        $file_path = base_path("help/{$file}");

        if(file_exists($file_path)){
            return file_get_contents($file_path);
        }else{
            return '<h1>File not found</h1>';
        }




    }

}
