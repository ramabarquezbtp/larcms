<?php

namespace App\Http\Controllers\admin;

use phpDocumentor\Reflection\Types\Parent_;
use Request;
use App\Http\Controllers\Controller;

class FormController extends Controller
{

    private $sort_field = 'name';
    private $sort_order = 'asc';
    private $main_table = 'forms';
    private $controller = 'forms';

    public function __construct()
    {
        //Parent_::__construct();
        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $keyword = Request::get('keyword');

        $where = "1";

        $session_keyword_key = "{$this->controller}.keyword";


        if(Request::has('keyword') || Request::get('keyword')){
            $keyword = Request::get('keyword');
            Request::session()->put($session_keyword_key, $keyword);
        }elseif(Request::session()->get($session_keyword_key)){
            $keyword = Request::session()->get($session_keyword_key);
        }

        if($keyword){
            $where = "name LIKE '%{$keyword}%' ";
        }

        $config = [
            'controller' => $this->controller,
            'sort_header' => [
                'name' => ['Name', 'asc', ''],
                'ctr' => ['No. of Submissions', 'asc', ''],
                'created_at' => ['Created at', 'asc', ''],
                'updated_at' => ['Updated at', 'asc', '']
            ]
        ];

        $sorter = new \App\Lib\Sorter($config, $this->sort_field, $this->sort_order);

        $res = \App\Model\Form::orderBy($this->sort_field, $this->sort_order)
            ->select(\DB::raw('forms.*, (select count(form_submissions.id) from form_submissions where form_submissions.form_id = forms.id) as ctr'))
            ->whereRaw($where)
            ->paginate(env('ADMIN_PAGE_LIMIT'));

        $data = [
            'rec' => $res,
            'sort_field' => $this->sort_field,
            'sort_order' => $this->sort_order,
            'keyword' => $keyword,
            'header' => $sorter->getHeader(),
            'controller' => $this->controller
        ];

        return view('admin.form.list',$data);
    }

    public function testMode()
    {
        session(['form'=>[]]);
        p(Request::session()->pull('form'));
        //p(session());
        die('session dead');
    }

    public function addPage($index,$id = 0){
        $redirect = 'admin/forms/create/'.$index;
        $session_index = 'form.new.'.$index;

        if($id){
            $redirect = "admin/forms/{$id}/edit/{$index}";
            $session_index = "form.{$id}.{$index}";
        }

        Request::session()->put($session_index,[]);
        return redirect($redirect);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($index = 1)
    {
        $session_index = 'form.new.'.$index;
        $session_form_name_index = "form_name.new";

        $form = Request::session()->get($session_index,[]);
        $formForCount = Request::session()->get('form.new',[[]]);

        if(isset($form[0]) && count($form[0])){
            $form = json_decode($form);
            $form = $form->fields;
        }

        //p(Request::session()->all());

        $form_count = count($formForCount);

        $view_data = [
            'form' => $form,
            'current_page' => $index,
            'page_count' => $form_count,
            'controller' => $this->controller,
            'form_name' => Request::session()->get($session_form_name_index,'')
        ];

        return view('admin.form.fbCreate', $view_data);
    }

    /**
     *
     */
    public function storeSession(){
        $index = Request::input('form_page_index');
        $payload = Request::input('payload');
        $id = Request::input('form_id');


        $session_index = 'form.new.'.$index;
        $session_form_name_index = "form_name.new";

        if($id){
            $session_index = "form.{$id}.{$index}";
            $session_form_name_index = "form_name.{$index}";
        }

        Request::session()->put($session_index,$payload);
        //Request::session()->put($session_index.'.form_name',Request::input('form_name'));

        Request::session()->put($session_form_name_index,Request::input('form_name'));

        echo $session_index;
    }

    public function removeForm($index = 1, $id = 0){

        $session_index = "form.new.{$index}";
        $session_index_main = "form.new";

        if($id){
            $session_index = "form.{$id}.{$index}";
            $session_index_main = "form.{$id}";
        }


        Request::session()->pull($session_index);

        $remaining = Request::session()->get($session_index_main);

        $ctr = 1;
        $source = [];

        foreach ($remaining as $row){
            $source[$ctr] = $row;
            $ctr++;
        }

        Request::session()->put($session_index_main,$source);

        $redirect = 'admin/forms/create/1';

        if($id){
            $redirect = 'admin/forms/' . $id . '/edit/1';
        }

        return redirect($redirect);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = new \App\Model\Form();

        $content = [
            'name' => Request::input('name'),
            'action' => 'form/submit',
            'method' => 'post'
        ];

        $form = Request::session()->pull("form.new",[]);
        $form_name = Request::session()->pull("form_name.new",[]);

        if(count($form) > 0){
            foreach ($form as $row){
                if(isJson($row)){
                    $content['tabs'][] = json_decode($row);
                }
            }
        }

        $data->name = Request::input('name');
        $data->content = json_encode($content);

        $data->save();

        return redirect('admin/forms')->with('success', config('app.alert_messages.save_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data = \App\Model\Form::find($id);

        $formObj = new \App\Lib\Form(json_decode($data->content), $data->id);

        $view_data = [
            'reCaptcha' => '<script src="https://www.google.com/recaptcha/api.js?render='.env('SITE_KEY').'"></script>',
            'form' => $formObj->generate(),
            'form_id' => $id
        ];

        return view('admin.form.view',$view_data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$index = 1)
    {

        $session_index = "form.{$id}.{$index}";
        $session_index_main = "form.{$id}";
        $session_form_name_index = "form_name.{$id}";

        $form = Request::session()->get($session_index,[]);
        $forForCount = Request::session()->get($session_index_main,-1);

        //p(Request::session()->all(),1);

        if(count($form) > 0 && !is_object($form)){
            $form = json_decode($form)->fields;
        }elseif(isset($form->fields)){
            $form = $form->fields;
        }

        $data = \App\Model\Form::find($id);

        //p(json_decode($data->content),1);

        if(isJson($data->content)){
            $default = json_decode($data->content);

            $default = $default->tabs;

            if(isJson($default)){
                $default = json_decode($default);
            }

        }

        if(($forForCount == -1 || count($forForCount) == 0) && count($default) > 0){

            $forForCount = [];

            foreach ($default as $i => $row){//die;
                $json = $row;
                $the_index = $session_index_main.'.'.($i+1);
                Request::session()->put($the_index,$json);
                $forForCount[] = $json;
            }

            if(isJson($default[($index-1)])){
                $form = json_decode($default[($index-1)])->fields;
            }else{
                $form = $default[($index-1)]->fields;
            }
        }

        $form_count = count($forForCount);

        $view_data = [
            'form' => $form,
            'current_page' => $index,
            'page_count' => $form_count,
            'name' => $data->name,
            'id' => $data->id,
            'controller' => $this->controller,
            'form_name' => Request::session()->get($session_form_name_index,$data->name)
        ];

        return view('admin.form.fbEdit', $view_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function save_changes(){
        $id = Request::input('form_id');
        $message = config('app.alert_messages.record_not_found');
        $data = \App\Model\Form::find($id);

        if(isset($data->id) && $data->id){

            $content = [
                'name' => Request::input('name'),
                'action' => 'form/submit',
                'method' => 'post'
            ];

            $form = Request::session()->get("form.{$id}",[]);

            if(count($form) > 0){
                foreach ($form as $row){

                    $content['tabs'][] = $row;

                }
            }

            $data->name = Request::input('name');
            $data->content = json_encode($content);
            $data->save();
            session(['form'=>[$id=>[]]]);
            $message = config('app.alert_messages.update_success');
        }
        return redirect('/admin/forms')->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function remove(){

        $data =  \App\Model\Form::find(Request::input('id'));
        $message = config('app.alert_messages.record_not_found');
        if(isset($data->id) && $data->id){

            \App\Model\FormSubmission::where('form_id',$data->id)->delete();

            $data->delete();
            $message = config('app.alert_messages.delete_success');
        }

        return redirect('/admin/forms')->with('success', $message);
    }

    public function formSubmission($form_id){
        $data = \App\Model\FormSubmission::where('form_id',$form_id)->orderBy('created_at','desc')->get();
        return view('admin.form.submission_list',['data' => $data, 'form_id' => $form_id]);
    }

    public function submissionView($id){
        $data = \App\Model\FormSubmission::find($id);

        //p(json_decode($data->content,128),1);

        //p(json_decode($data->content,128),1);
        //p(json_encode($data,128),1);
        return view('admin.form.submission',['data' => $data]);
    }
}
