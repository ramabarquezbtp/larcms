<?php

namespace App\Http\Controllers\admin;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Request;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    private $sort_field = 'created_at';
    private $sort_order = 'desc';
    private $main_table = 'events';
    private $controller = 'events';
    private $view_folder = 'event';
    private $mgt_name = 'Events';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $keyword = Request::get('keyword');

        $where = "1";

        if($keyword){
            $where = "title LIKE '%{$keyword}%' ";
        }

        $config = [
            'controller' => $this->controller,
            'sort_header' => [
                'title' => ['Title', 'asc', ''],
                'featured' => ['Featured', 'asc', ''],
                'published' => ['Published', 'asc', ''],
                'date' => ['Event Date', 'asc', ''],
                'created_at' => ['Created at', 'asc', ''],
                'updated_at' => ['Updated at', 'asc', '']
            ]
        ];

        $sorter = new \App\Lib\Sorter($config, $this->sort_field, $this->sort_order);

        $res = \App\Model\Event::orderBy($this->sort_field, $this->sort_order)
            ->whereRaw($where)
            ->paginate(env('ADMIN_PAGE_LIMIT'));

        //p($res[0]->pages(),1);

        $data = [
            'rec' => $res,
            'sort_field' => $this->sort_field,
            'sort_order' => $this->sort_order,
            'header' => $sorter->getHeader(),
            'controller' => $this->controller,
            'mgt_name' => $this->mgt_name,
            'keyword' => $keyword
        ];

        return view('admin.'.$this->view_folder.'.list',$data);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $view_data = [
            'mgt_name' => $this->mgt_name,
            'controller' => $this->controller
        ];
        return view('admin.'.$this->view_folder.'.create',$view_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $obj = new \App\Model\Event();

        $obj->title = Request::input('title');
        $obj->uri = Request::input('uri');
        $obj->picture_link = Request::input('picture_link');
        $obj->summary_content = Request::input('summary_content');
        $obj->full_content = Request::input('content');
        $obj->date = Request::input('date');
        $obj->featured = 0;
        $obj->published = 0;

        if(Request::input('featured')){
            $obj->featured = 1;
        }

        if(Request::input('published')){
            $obj->published = 1;
        }

        $obj->save();

        return redirect('admin/' . $this->controller)->with('success', config('app.alert_messages.save_success'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $obj = \App\Model\Event::find($id);

        //p($content->title);

        $view_data = [
            'rec' => $obj,
            'controller' => $this->controller,
            'mgt_name' => $this->mgt_name
        ];

        return view("admin.{$this->view_folder}.edit",$view_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        p($id);

        //
        return 'you';
        //p(Request::get('title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save_changes(){
        $id = Request::input('id');
        $message = config('app.alert_messages.record_not_found');
        $obj = \App\Model\Event::find($id);

        if(isset($obj->id)){

            $obj->title = Request::input('title');
            $obj->uri = Request::input('uri');
            $obj->picture_link = Request::input('picture_link');
            $obj->summary_content = Request::input('summary_content');
            $obj->full_content = Request::input('content');
            $obj->date = Request::input('date');
            $obj->featured = 0;
            $obj->published = 0;

            if(Request::input('featured')){
                $obj->featured = 1;
            }

            if(Request::input('published')){
                $obj->published = 1;
            }

            $obj->save();
            $message = config('app.alert_messages.update_success');
        }
        return redirect('/admin/' . $this->controller)->with('success', $message);
    }

    public function remove(){

        $obj =  \App\Model\Event::find(Request::input('id'));
        $message = config('app.alert_messages.record_not_found');
        if(isset($obj->id) && $obj->id){
            $obj->delete();
            $message = config('app.alert_messages.delete_success');
        }

        return redirect('/admin/' . $this->controller)->with('success', $message);
    }
}
