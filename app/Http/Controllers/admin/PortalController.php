<?php

namespace App\Http\Controllers\admin;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Request;
use App\Http\Controllers\Controller;

class PortalController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function page($page_id = 'Home'){

        $page = \App\Model\Page::where('uri','/'.$page_id)->first();

        if(!isset($page->id)){
            $page = \App\Model\Page::where('uri','/page-not-found')->first();
        }

        $reCaptcha = '';

        $keyword = Request::get('s');

        $search_content = '';

        if($keyword){
            $search_content = $this->find_keyword();
        }

        $view = 'public.not-found';
        $data = [];

        if(isset($page->id)){
            $view = 'public.page';

            $page = \App\Model\Page::find($page->id);

            if(isset($page->page_content) && $page->page_content){

                foreach ($page->page_content as $selected_content){

                    if($selected_content->form_id){

                        $formObj = new \App\Lib\Form($selected_content->form->content,$selected_content->form->id);

                        $content = [
                            'content' => $formObj->generate()
                        ];

                        $data['Inner'][$selected_content->order][] = (object)$content;

                        if($reCaptcha === ''){
                            $reCaptcha = '<script src="https://www.google.com/recaptcha/api.js?render='.env('SITE_KEY').'"></script>';
                        }

                    }elseif($selected_content->gallery_id){

                        $rec = \App\Model\Gallery::find($selected_content->gallery_id);

                        //p(json_encode($rec,128),1);

                        $gallery = ['bullets'=>'','items'=>''];

                        $active[0] = 'active';

                        $style = 'style="width:25%; margin: 0; padding: 0"';
                        $imageStyle = 'style="width:100%;"';


                        foreach ($rec->gallery_photo as $index => $row){
                            //$gallery['bullets'] .= '<li data-target="#myCarousel" data-slide-to="'.$index.'" class="'.((isset($active[$index]))?'active':'').'"></li>';
                            $gallery['items'] .= '<li class="col-xs-6 col-sm-4 col-md-3" data-responsive="" data-src="'.url('files/image/' . $row->file_id).'" data-sub-html="" '.$style.'>
                                    <a href="" '.$imageStyle.'>
                                        <img class="img-responsive" src="'.url('files/thumbnail/' . $row->file_id).'" '.$imageStyle.'>
                                    </a>
                                </li>';
                        }

                        $content = [
                            'content' => view("admin.gallery.images",['gallery' => $gallery])
                        ];

                        $data['Inner'][$selected_content->order][] = (object)$content;
                    }else{

                        $content_type = $selected_content->content->content_type->name;

                        $content = $selected_content->content;

                        if($content_type != 'Inner'){
                            $data[$content_type] = $content;
                        }else{
                            if($search_content){

                                $content->content = $search_content;
                            }

                            $content->content = str_replace("/page/","/portal/",$content->content);

                            $data[$content_type][$selected_content->order][] = $content;

                            //die($content);

                        }
                    }
                }
                if(isset($data['Inner'])){
                    ksort($data['Inner']);
                }
            }

            $data['page'] = $page;
            $data['reCaptcha'] =  $reCaptcha;
        }

        return view($view,$data);
    }
}
